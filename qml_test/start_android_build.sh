#!/bin/sh

# build this sample as an android application. apks will be in 'apk' folder.

# set CONTAINER_IMAGE if you wish to use a different container


set -e

: "${CONTAINER_IMAGE:=registry.gitlab.com/etherlab.org/build-container-factory/android:pdqmlwidgets}"

# this is the apk output directory
mkdir -p apk

# start container and capture its id
container_id=$(docker run -it --rm --detach --volume $PWD:/project:ro $CONTAINER_IMAGE)

set +e
# execute the build script inside the container
docker exec $container_id /bin/bash /project/this_builds_android_in_container.sh
build_retcode=$?
# copy the apks if it was successful
[[ $build_retcode -eq 0 ]] && docker cp ${container_id}:/tmp/apk/. apk/
# stop the container
docker stop $container_id
exit $build_retcode
