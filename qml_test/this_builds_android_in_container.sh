#!/bin/sh

set -ex

# prepare expected directory structure
mkdir -p src build-arm build-arm64
ln -s /project src/qml_test
# compile
/opt/helpers/build-cmake qml_test $PWD/src/qml_test \
            -DQTANDROID_EXPORTED_TARGET="QmlTest" \
            -DANDROID_APK_DIR=$PWD/src/qml_test/android
# convert to apk
/opt/helpers/create-apk qml_test
# copy back to user
mkdir -p /tmp/apk
find . -name QmlTest-arm*.apk -exec cp {} /tmp/apk/ \;
echo done...
