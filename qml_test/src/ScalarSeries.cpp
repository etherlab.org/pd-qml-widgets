/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ScalarSeries.h"
#include <QtCharts/QAbstractSeries>
#include <QtDebug>

//using Pd::ScalarSeries;

QT_CHARTS_USE_NAMESPACE

#define DEFAULT_TIMERANGE  10.0

/****************************************************************************/

/** Constructor.
 */
ScalarSeries::ScalarSeries():
    process(0),
    path(""),
    period(QtPdCom::event_mode),
    _scale(1.0),
    _offset(0.0),
    timeRange(DEFAULT_TIMERANGE),
    dataPresent(false)
{
}

/****************************************************************************/

/** Destructor.
 */
ScalarSeries::~ScalarSeries()
{
}

/****************************************************************************/

void ScalarSeries::updateConnection()
{
    // FIXME test for scalar here
    if (process and process->isConnected()) {
      //we first allow only scalar elements as selector
      QStringList pathElements = path.split('#',QString::SkipEmptyParts);

      if(pathElements.count() > 1) {
	bool ok;
	int index = pathElements.at(1).toInt(&ok);
	if(!ok) {
	  qCritical() << "Only integer as path selector allowed currently!" <<
	    "Not registering the variable: " << path;
	  return;
	}
	PdCom::ScalarSelector selector({index});
	//qDebug() << "setvariable path " << pathElements.at(0) << " ,index " << index;
	setVariable(process, pathElements.at(0), selector, period, _scale, _offset);
      } else {
	//PdCom::Selector all;
	setVariable(process, path,{}, period, _scale, _offset);
      }
    }
}

/****************************************************************************/

void ScalarSeries::setProcess(QtPdCom::Process *value){

  //  qDebug() << "set Process" << value;
  if(value != process) {
    if(process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(process,0,0,0);
    }
    if(value) {
      process = value;
      QObject::connect(process, SIGNAL(processConnected()), this, SLOT(processConnected()));
      QObject::connect(process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(process, SIGNAL(error()), this, SLOT(processError()));
    }
  }
}

/****************************************************************************/

void ScalarSeries::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void ScalarSeries::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void ScalarSeries::processError()
{
  clearData();
}

/****************************************************************************/

QVariant ScalarSeries::getConnection(){
  QVariantMap e;

  e["path"] = path;
  e["period"] = period.getInterval();
  e["offset"] = _offset;
  e["scale"] = _scale;
  return QVariant::fromValue(e);
}

/****************************************************************************/

void ScalarSeries::setConnection(QVariant value){

  if (value.canConvert<QVariantMap>()) {
    QVariantMap connection = value.toMap();

    if(connection.contains("process")) {
      QtPdCom::Process *process = connection["process"].value<QtPdCom::Process *>();
      setProcess(process);
    } else { //try default process
      setProcess(QtPdCom::Process::getDefaultProcess());
    }

    connection.contains("path")?
      path = connection["path"].toString():
      path = "";

    period = QtPdCom::event_mode;
    //"sampleTime"? backward compatibility
    if (connection.contains("sampleTime")) {
      period = QtPdCom::Transmission(std::chrono::duration<double>(
        connection["sampleTime"].toDouble())
      );
    }

    if (connection.contains("period")) {
      period = QtPdCom::Transmission(std::chrono::duration<double>(
        connection["period"].toDouble())
      );
    }

    connection.contains("offset")?
      _offset = connection["offset"].toDouble():
      _offset = 0;

    connection.contains("scale")?
      _scale = connection["scale"].toDouble():
      _scale = 1.0;

    updateConnection();
    emit connectionUpdated();

  } else {
    qCritical() << "connection has to be a map";
  }
}

/****************************************************************************/

void ScalarSeries::setPath(QString &value){
   if(value != path) {
    path = value;
    period = QtPdCom::event_mode;
    _offset = 0;
    _scale = 1;
    //the design choice is, that setting only the path is for
    //event transmitted variables from the defaultProcess
    setProcess(QtPdCom::Process::getDefaultProcess());
    updateConnection();
    emit pathChanged(path);
  }
}

/****************************************************************************/

void ScalarSeries::clearData()
{
    dataPresent = false;
    values.clear();

    emit dataPresentChanged(dataPresent);
}

/****************************************************************************/

void ScalarSeries::setTimeRange(double _range)
{
  timeRange = _range;
  std::chrono::nanoseconds range{(int64_t) (1.1 * _range * 1.0e9)};
  /* 1.1: hold some more values than necessary for synchronisation reasons. */
  values.setRange(range);
  emit timeRangeChanged();
}


double ScalarSeries::getTimeRange() const
{
    return timeRange;
}


/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void ScalarSeries::newValues(std::chrono::nanoseconds ts)
{
    double value;
    PdCom::details::copyData(&value,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    value = value * scale + offset;
    //qDebug() << "value " << value <<" t " << std::chrono::duration<double>(ts).count();
    values.append(ts, value);
}

/****************************************************************************/
//see: /vol/opt/Qt5.10-android/Examples/Qt-5.10.1/charts/qmloscilloscope

void ScalarSeries::update(QAbstractSeries *series){

  if (series && values.getLength() > 1) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);

	QVector<QPointF> points(values.getLength());

	std::chrono::nanoseconds toffset =
	  values[values.getLength()-1].first;

	for (uint i = 0; i < values.getLength(); i++) {
	  ValueRing<double>::TimeValuePair value = values[i];
	  double t = std::chrono::duration<double>(value.first - toffset).count();
	  points[i].setX(t);
	  points[i].setY(value.second);

	}
	xySeries->replace(points);
  }
}
/****************************************************************************/
