/*****************************************************************************
 *
 * Copyright (C) 2018-2021  Wilhelm Hagemeister<hm@igh.de>,
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SCALARSERIES_H
#define PD_SCALARSERIES_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
#include <QXYSeries>

QT_CHARTS_USE_NAMESPACE

#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/Process.h>
#include <PdQmlWidgets2/ValueRing.h>

using PdQmlWidgets::ValueRing;

/****************************************************************************/

/** Scalar Series to be used in QML applications
    which is aware of the process to detect connections/or reconnect.
    It supplies a series function to be used in QML ChartViews
 */

class ScalarSeries:
  public QObject, public QtPdCom::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    /** if one want's to update the variable connection values in one call
       supply a map with {"path":path,
			  "period":sampleTime
                          ....
                         }
     */
    Q_PROPERTY(QVariant connection READ getConnection WRITE setConnection
	       NOTIFY connectionUpdated)

    /* indicates that the process is connected and data is transfered */
    Q_PROPERTY(bool connected READ getDataPresent
	       NOTIFY dataPresentChanged)

    Q_PROPERTY(double timeRange READ getTimeRange WRITE setTimeRange
	       NOTIFY timeRangeChanged)
    public:
        ScalarSeries();
        virtual ~ScalarSeries();

	QString getPath() const { return path; };
	void setPath(QString &);

	bool getDataPresent() { return dataPresent; };
	QVariant getConnection();
	void setConnection(QVariant);

        void clearData(); // pure-virtual from ScalarSubscriber
        Q_INVOKABLE bool hasData() const { return dataPresent; };
	QtPdCom::Process *getProcess() const { return process; };

	void setProcess(QtPdCom::Process *);

	std::chrono::nanoseconds getMTime() const { return mTime; };
	double getMTimeToDouble() {
	  return std::chrono::duration<double>(mTime).count();
	};

	double getTimeRange() const;
        void setTimeRange(double);

    private:
	QtPdCom::Process *process;
        QVariant value; /**< Current value. */
        QString path;
	QtPdCom::Transmission period;
	double _scale;
	double _offset;
    std::chrono::nanoseconds mTime; /**< Modification Time of Current value. */
        double timeRange; /**< Time range. */
        bool dataPresent; /**< There is a process value to display. */
        ValueRing<double> values; /**< Ring buffer with time/value
                                    pairs. */
	void updateConnection(); /**< (re)connects to variable */

    // pure-virtual from ScalarSubscriber
    void newValues(std::chrono::nanoseconds);

    template <typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, void>::type
    copyData(T &dest) const
    {
        PdCom::details::copyData(
                &dest, PdCom::details::TypeInfoTraits<T>::type_info.type,
                getData(),
                getVariable().getTypeInfo().type, 1);
    }

    public slots:
      void update(QAbstractSeries *series); /**< send data to the Chart */

    private slots:
        void processConnected();
	void processDisconnected();
	void processError();

    signals:
	void pathChanged(QString &);
	void dataPresentChanged(bool);
	void connectionUpdated();
        void timeRangeChanged();
};

/****************************************************************************/

#endif
