/****************************************************************************
**
** Copyright (C) 2019 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** project: pdWidgets
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4

import de.igh.pd 2.0
import de.igh.svg 1.0



Item {
    LiveSvg {
        id:svg
        anchors.fill: parent
        source: "qrc:/images/svg_example.svg"
	invert: appContainer.Material.theme == Material.Dark
	Button {
	    id:myButton
	    objectName:"myButton"
	    text:"press me"
	}
	PdLed {
	    objectName:"squareRect1"
	    connection: {
		"path":"/osc/enable"
	    }
	    color:"red"
	    blinking:true
	    invert:true
	}
        PdDigital {
	    objectName:"processValue1"

            connection: {
		"path":"/Taskinfo/0/Period",
		"sampleTime":0.1,
		"scale":1000
	    }
            suffix:"ms"
            font.bold: true
            font.pixelSize:20
        }
	Label {
	    objectName:"nothingHappens"
	    text:"nothing happens"
	    visible:myButton.pressed
	}
    }
} //Item
