import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Dialog {
    title: qsTr("Verbinden mit...")

    parent: Overlay.overlay
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)

    standardButtons: Dialog.Ok | Dialog.Cancel | Dialog.RestoreDefaults
    modal: true
    contentWidth: col.implicitWidth
    contentHeight: col.implicitHeight

    property alias host:hostInput.text
    property alias port:portInput.value
	property alias useTLS: cbUseTLS.checked
    closePolicy:Popup.NoAutoClose | Popup.CloseOnEscape

    GridLayout {
	rows:1
	rowSpacing:20
	columnSpacing:20
	id:col
	Label {
	    text:"Host"
	}
	TextField {
	    id:hostInput
	    placeholderText:"localhost"
	    text:"localhost"
	    Layout.alignment:Qt.AlignRight
	}
	Item {
	    height:1
	    width:50
	    Layout.fillWidth:true
	}
	Label {
	    text:"Port"
	}
	SpinBox {
	    id:portInput
		editable: true
	    from:2345
	    value:2345
	    to:10000
	}
	CheckBox {
		id: cbUseTLS
		text: qsTr("Use TLS")
	    Layout.fillWidth:true //alignment:Qt.alignRight
	}
    } // GridLayout
    onReset: {
		hostInput.text = "localhost";
		portInput.value = 2345;
    }

}
