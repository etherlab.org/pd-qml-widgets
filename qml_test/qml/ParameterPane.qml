/****************************************************************************
**
** Copyright (C) 2019 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** project: pdWidgets
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4

import de.igh.pd 2.3



Item {
    RowLayout {
            id:motorLayout
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 10
        spacing: 5

        GroupBox {
            title: "Parameters"
            Layout.fillHeight: true
            Layout.minimumWidth:parent.width/2
            ColumnLayout {
                Label{
                    text:"Touchedit"
                }
                //create this control 3 times ....
                Repeater{
		    id:repeater
                    model:3
		    property var header:["Winkel","Weg","Druck"]
		    property var suffix:["m/s","kg","bar"]
		    
                    RowLayout { 
                        //Layout.fillWidth: true
                        Label { text: "Dummy"; Layout.fillWidth:true; }
                        PdTouchEdit {
                            font.pixelSize:16
                            Layout.preferredWidth:80
                            decimals:2
                            suffix: repeater.suffix[modelData]
			    title: repeater.header[modelData]
                            lowerLimit:1
                            upperLimit:10
                            scale:1
                            path:"/osc/omega" 
                        }
                    }
                }

                Label{
                    text:"Slider"
                }
                PdSlider {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillHeight:true
                    id:control
                    path:"/osc/omega"
                    from: 1
                    to: 10
                    snapMode:Slider.SnapAlways
                    stepSize:1
                    padding:20
                    orientation:Qt.Horizontal
                    //tickmarksEnabled : true
                    ToolTip {
                        parent: control.handle
                        visible: control.pressed
                        text: control.value.toFixed(1)+" "
                        font.pixelSize:20
                    }

                } 
                Label {
                    text:"Button"
                }

                PdButton {
                    id:incBtn
                    text:"Increment"
                    icon.source:"qrc:/images/arrow-up.svg" 
                    icon.width:10
                    icon.height:20
		    event:true
                    path:"/osc/omega"
                }
		
                //Helper variable for button
                PdScalar {
                    id:btnVariable
                    path:"/osc/omega"
                }
                Button {
                    id:decBtn
                    text:"Decrement"
                    icon.source:"qrc:/images/arrow-down.svg" 
                    icon.width:10
                    icon.height:20
                    onPressed:{ 
                        //there is no dec() function
                        btnVariable.value-=1 
                    }
                }
		/* Example Model with indexRole for PdComboBox */
		ListModel {
		    id:model1
		    ListElement {key: "None"; value:0}
		    ListElement {key: "a few"; value:2}
		    ListElement {key: "many"; value:4} 
		    ListElement {key: "all"; value:6} 
		}

		PdComboBox {
		    indexRole:"value"
		    textRole:"key"
		    model:model1
                    path:"/osc/omega"
		}

		/* Example Model without indexRole */
		ListModel {
		    id:model2
		    ListElement {key: "None"}
		    ListElement {key: "a few"}
		    ListElement {key: "many"} 
		    ListElement {key: "all"} 
		}

		PdComboBox {
		    textRole:"key"
		    model:model2
                    path:"/osc/omega"
		    
		}
		
            } //ColumnLayout
        } //Groupbox1

        GroupBox { 
            title: "Parameters2"
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout {
                Label {
                    text:"Enable"
                }
                //create this control 2 times ....
                Repeater{
                    model:2
                    PdCheckBox{
                        path:"/osc/enable" 
                    }
                }

                Label {
                    text:"reset"
                }
                PdCheckBox{
                    path:"/osc/reset" 
                    invert:true
                }

                Label {
                    text:"Switch"
                }
		
                PdSwitch{
                    path:"/osc/reset" 
                }
		
		PdButton {
		    text:"checkable"
		    checkable:true
                    path:"/osc/reset" 
		}

                Label {
                    text:"Label"
                }
                PdLabel {
                    path:"/osc/omega" 
                    font.pixelSize:20
                    font.bold: true
                    Component.onCompleted: {
                        hash[0] = "Null";
                        hash[1] = "Eins";
                        hash[2] = "Zwei";
                        hash[3] = "Drei";
                    }
                }
                Label {
                    text:"PdText"
                }
                PdText {
                    path:"/osc/omega" 
                    font.pixelSize:20
		    prefix:"Value: <b>"
		    suffix:"</b>"
		    textRole:"text"
		    undefText:"key undefined"
		    map:{
			const map = new Map()
			map.set(0,{"color":"red","text":"Car"});
			map.set(1,{"color":"green","text":"Person"});
			map.set(2,{"color":"green","text":"Animal"});
			map.set(3,{"color":"#FF9800","text":"nothing"});
			return map;
		    } 
                }
            } //Columnlayout
        } //GroupBox
    } //RowLayout
} //Item
