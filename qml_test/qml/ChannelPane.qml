/****************************************************************************
**
** Copyright (C) 2019 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** project: pdwidgets
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4

import de.igh.pd 2.3

Item {
    RowLayout {
            id:motorLayout
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 10
            spacing: 5


        GroupBox { 
            title: "Values"
            //spacing:2
            Layout.fillHeight: true
            ColumnLayout {
                Label{
                    text:"Digital"
                }

                RowLayout {
                    Label {text: "Period"; Layout.fillWidth:true; }
                    PdDigital {
			connection: {
                            "path":"/Taskinfo/0/Period",
                            "period":0.1,
                            "scale":1000
			}
                        suffix:"ms"
                        font.bold: true
                        font.pixelSize:20
                        horizontalAlignment:Text.AlignRight
                        Layout.fillWidth:true;
                    }
                }

                RowLayout {
                    Label {text: "Oszillator"; Layout.fillWidth:true; }
                    PdVectorDigital {
			connection: {
                            "path":"/osc/derivative",
                            "period":0.1,
                            "scale":1
			}
			decimals:0
                        suffix:" v"
                        font.bold: true
                        font.pixelSize:20
                        mode: Math.max(0,modeBox.curValue)
                        index: modeBox.curValue > -1? -1:-1*modeBox.curValue - 1

                        horizontalAlignment:Text.AlignRight
                        Layout.fillWidth:true;
                    }
                }
/*
                RowLayout {
                    Label {text: "Oszillator"; Layout.fillWidth:true; }
                    PdDigital {
			connection: {
                            "path":"/osc/derivative#0",
                            "period":0.1,
                            "scale":1
			}
			decimals:0
                        suffix:" v"
                        font.bold: true
                        font.pixelSize:20
                        horizontalAlignment:Text.AlignRight
                        Layout.fillWidth:true;
                    }
                    PdDigital {
			connection: {
                            "path":"/osc/derivative#1",
                            "period":0.1,
                            "scale":1
			}
			decimals:0
                        suffix:" v"
                        font.bold: true
                        font.pixelSize:20
                        horizontalAlignment:Text.AlignRight
                        Layout.fillWidth:true;
                    }
                }
*/
                RowLayout {
                    Label {text: "Bar"; Layout.fillWidth:true;}
                    PdBar {
                        Layout.fillWidth:true;
                        from:-20
                        to:20
                        suffix:" N"
                        decimals:2
			connection: {
			    "period":0.1,
                            "path":"/osc/cos"
			}
                    }
                }

                RowLayout {
                    Label { text: "Digital with vector"; Layout.fillWidth:true; }
                    ListModel {
                        id:modeModel
                        ListElement {key: "All"; value:0}
                        ListElement {key: "Max/Min"; value:1}
                        ListElement {key: "Mean/Std"; value:2} 
                        ListElement {key: "Set"; value:3} 
                    }
                    ComboBox {
                        id:modeBox
                        textRole:"key"
                        model:modeModel
                        currentIndex:1
                        //helper variable
                        property int curValue:modeModel.get(modeBox.currentIndex).value
                    }
                }

                PdDigital {
                    id:voltage
		    connection: {
			"path":"/SawTooth",
			"period":0.1
		    }
                    suffix:""
                    font.bold: true
                    font.pixelSize:20
                    horizontalAlignment:Text.AlignRight
                    Layout.fillWidth:true;
                    //mode:Math.max(0,modeBox.curValue)
                }

                Label {text: "Vector Indicator"; Layout.fillWidth:true;}
/*                PdVectorLed {
                    Layout.alignment:Qt.AlignHCenter
		    connection: {
			"path":"/osc/sin",
			"period":0.1
		    }
                    color:Material.accent
                }
*/

                Label {text: "Vector Indicator with color hash"; Layout.fillWidth:true;}

                PdSlider {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillHeight:true
                    id:control
                    path:"/osc/amplitude/Setpoint"
                    from: 1
                    to: 10
                    snapMode:Slider.SnapAlways
                    stepSize:1
                    padding:20
                    orientation:Qt.Horizontal
                    //tickmarksEnabled : true
                    ToolTip {
                        parent: control.handle
                        visible: control.pressed
                        text: control.value.toFixed(1)+" "
                        font.pixelSize:20
                    }
                } 
/* later
                PdVectorLed {
                    id:indicator2
                    Layout.alignment:Qt.AlignHCenter
                    color:Material.accent
                    path:"/osc/amplitude/Setpoint"
                    Component.onCompleted: {
                        colorHash[-1] = "red"; 
                        colorHash[0] = "black";  
                        colorHash[1] = "yellow"; 
                        colorHash[2] = "#FF9800"; 
                        colorHash[3] = "#4CAF50"; 
                        colorHash[4] = "blue"; 
                    }
                    blinking:indicator2.value == 1
                }
*/
		PdBar {
                    Layout.fillHeight:true;
		    Layout.minimumWidth:100
                    from:-20
                    to:40
		    fontPixelSize:16
                    suffix:" N"
                    decimals:2
		    orientation:Qt.Vertical
		    connection: {
			"period":0.1,
			"path":"/osc/cos"
		    }
		    //startZero:false
		    //labelVisible:false
                }

            } //ColumnLayout
        } //Groupbox Digitals

        GroupBox {
            title: "Graphs"
            Layout.fillWidth:true
            Layout.fillHeight: true
            padding:0
            
            ColumnLayout {
                spacing:0
                anchors.fill: parent
                //example for dynamic loading
                Loader { 
                    id:chartView
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                }
            } //Columnlayout
        } //GroupBox
    } //RowLayout
    Component.onCompleted: {
            chartView.setSource("qrc:/qml/ChartsPane.qml");
    }
} //Item
