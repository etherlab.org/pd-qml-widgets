#!/bin/sh

set -e
set -x

if [ -z "$1" ]; then
        echo "Usage: $0 <outdir>"
        exit 1
fi

mkdir -p $1
cd $1
cp -r /usr/x86_64-w64-mingw32/sys-root/mingw/lib/qt5/plugins/* .
cp -r /usr/x86_64-w64-mingw32/sys-root/mingw/lib/qt5/qml .
cp -r /usr/x86_64-w64-mingw32/sys-root/mingw/bin/*.dll .

echo -e "[Paths] \nPrefix=." > qt.conf

echo "removing uncessary files"
rm -rf printsupport sqldrivers designer bearer generic qmltooling platfromthemes
rm -rf qml/qmltooling qml/QtTest
rm -rf qml/QtQuick/Controls.2/designer
rm -rf qml/QtQuick/Extras/designer
rm -rf libcairo-2.dll libcairo-gobject-2.dll libcairo-script-interpreter-2.dll libdbus-1-3.dll libsqlite3-0.dll
rm -rf Qt5Concurrent.dll Qt5DBus.dll Qt5DesignerComponents.dll Qt5Designer.dll   Qt5PrintSupport.dll Qt5QuickParticles.dll Qt5QuickShapes.dll Qt5QuickTest.dll Qt5Sql.dll
