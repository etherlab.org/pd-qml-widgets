#-----------------------------------------------------------------------------
#
# Copyright (C) 2020  Wilhelm Hagemeister Pose <hm@igh.de>
#
# This file is part of the PdQmlWidgets library.
#
# The PdQmlWidgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The PdQmlWidgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the PdQmlWidgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
#
#-----------------------------------------------------------------------------

TEMPLATE = app
TARGET = pdqmltest
QT += charts qml quick quickcontrols2
QT += svg #QSvgRenderer
QT += xml #QDomDocument

#CONFIG+=qml_debug

MOC_DIR = .moc
OBJECTS_DIR = .obj

DEPENDPATH += ..
INCLUDEPATH += ..

LIBDIR = "lib"
contains(QMAKE_HOST.arch, x86_64): {
    LIBDIR = "lib64"
}

!isEmpty(PDCOMPREFIX) {
    INCLUDEPATH += $${PDCOMPREFIX}/include
    LIBS += -L$${PDCOMPREFIX}/lib$${LIBEXT}
}

QMAKE_LFLAGS += -L$$OUT_PWD/..
QMAKE_LFLAGS_APP += -Wl,--rpath -Wl,..

LIBS += -lPdQmlWidgets2 -lQtPdCom1

LIBS += -lpdcom5 -lexpat

HEADERS += \
    src/ScalarSeries.h

SOURCES += \
    src/main.cpp \
    src/ScalarSeries.cpp

OTHER_FILES += \
    qml/*.qml



RESOURCES += \
    resources.qrc

target.path = .

android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    DISTFILES += \
        android/AndroidManifest.xml \
        android/build.gradle \
        android/res/values/libs.xml

    ANDROID_EXTRA_LIBS += $${PDCOMPREFIX}/lib/libexpat.so
    ANDROID_EXTRA_LIBS += $${PDCOMPREFIX}/lib/libpdcom5.so
    ANDROID_EXTRA_LIBS += $${PDCOMPREFIX}/lib/libQtPdCom1.so
    ANDROID_EXTRA_LIBS += $${PDCOMPREFIX}/lib/libPdQmlWidgets2.so
}
