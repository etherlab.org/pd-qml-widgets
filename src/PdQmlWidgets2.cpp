/*****************************************************************************
 *
 * Copyright (C) 2018-2022  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <PdQmlWidgets2/PdQmlWidgets2.h>

#include <QtQml>
#include <QQmlEngine>
#include <PdQmlWidgets2/LiveSvg.h>
#include <PdQmlWidgets2/ScalarVariant.h>
#include <PdQmlWidgets2/VectorVariant.h>
#include <QtPdCom1/Process.h>
#include <QtPdCom1/LoginManager.h>
#include <QtPdCom1/MessageModel.h>


#include <git_revision_hash.h>


#define STR(x) #x
#define QUOTE(x) STR(x)

const char *const PdQmlWidgets::pdqmlwidgets_version_code =
        QUOTE(PDQMLWIDGETS_MAJOR) "." QUOTE(PDQMLWIDGETS_MINOR) "." QUOTE(PDQMLWIDGETS_RELEASE);

const char *const PdQmlWidgets::pdqmlwidgets_full_version = GIT_REV;

// explicitly load resource file
// this has to be at global namespace, so we use a trampoline
static void loadrcc() {
#ifdef USED_QTQUICK_COMPILER
    Q_INIT_RESOURCE(de_igh_pdQmlWidgets_qmlcache);
#endif
    Q_INIT_RESOURCE(de_igh_pdQmlWidgets);
}

namespace PdQmlWidgets {

void setQmlImportPathToResourceFile(QQmlEngine &engine)
{
    loadrcc();
    engine.addImportPath("qrc:///");
}

int registerScalarVariant()
{
    return qmlRegisterType<PdQmlWidgets::ScalarVariant>("de.igh.pd", 2, 0, "PdScalar");
}

int registerVectorVariant()
{
    return qmlRegisterType<PdQmlWidgets::VectorVariant>("de.igh.pd", 2, 0, "PdVector");
}

int registerLiveSvg()
{
    return qmlRegisterType<PdQmlWidgets::LiveSvg>("de.igh.svg", 1, 0, "LiveSvg");
}

void registerQtPdComTypes()
{
    qmlRegisterType<QtPdCom::Process>("de.igh.pd", 2, 0, "Process");
    qmlRegisterType<QtPdCom::MessageModel>("de.igh.pd", 2, 0, "MessageModel");
    qmlRegisterType<QtPdCom::LoginManager>("de.igh.pd", 2, 0, "LoginManager");
}

}  // namespace PdQmlWidgets
