/****************************************************************************
**
**
****************************************************************************/

#ifndef PD_LIVESVG_P_H
#define PD_LIVESVG_P_H

#include <QString>
#include <QSvgRenderer>
#include <QSize>
#include <QMouseEvent>
#include <QDomDocument>
#include <QDomElement>
#include <QTimer>
#include <QPixmap>
#include <QSizeF>
#include <QPointF>
#include <QtQuick/QQuickPaintedItem>

namespace PdQmlWidgets {

class LiveSvg;

class LiveSvgPrivate {
  public:
    LiveSvgPrivate(LiveSvg* );

  private:
    QDomDocument m_svgdoc;
    QDomDocument m_svgDynamicDoc; 
    QSvgRenderer m_renderer;
    QRectF viewBox;
    QPixmap backgroundPixmap; /**< Pixmap that stores the background. */
    QRectF rendererBounds;
    QList<QVariant> overlayElements;
    QString source;
    bool empty;  //* no pixmal
    bool invert;
    bool backgroundPixmapDirty;
    QDomElement
    findLayer(const QString &layerName, const QDomElement &parent);
    QQuickItem *findChildItem(QQuickItem *, const QString &);

    void findElementsWithAttribute(
            const QDomElement &elem,
            const QString &attr,
            QList<QDomElement> &foundElements);
    QDomElement findElementById(
	    const QDomElement &elem,
	    const QString &idValue);
    void changeAttribute(const QString &,const QString &,const QString &);

    void getOverlayRects(const QDomElement &parent);
    void getTransformations(const QDomNode &elem, QPointF &offset);

    void printElements(QList<QDomElement> elements);
    void printAttributes(QDomElement elem);
    void scaleQmlChildren(double, double, double, double, QSvgRenderer &);
    void getTransform(QSvgRenderer &);

  private:
    LiveSvg * const q_ptr;
    Q_DECLARE_PUBLIC(LiveSvg);
};

/****************************************************************************/

}  // namespace PdQmlWidgets

#endif
