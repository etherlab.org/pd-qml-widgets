/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>, with code from
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <PdQmlWidgets2/ScalarVariant.h>
#include "ScalarVariant_p.h"

using PdQmlWidgets::ScalarVariant;
using PdQmlWidgets::ScalarVariantPrivate;

#include <QtPdCom1/Transmission.h>

#include <QStringList>

#include <QtDebug>

//#define PD_DEBUG_SCALARVARIANT

/****************************************************************************/

/** Constructor.
 */
ScalarVariant::ScalarVariant(QObject *parent):
  QObject(parent),
  d_ptr(new ScalarVariantPrivate(this))
{
}

ScalarVariantPrivate::ScalarVariantPrivate(ScalarVariant *This) :
    q_ptr(This),
    process(0),
    value(0),
    path(""),
    period(0),
    _scale(1.0),
    _offset(0.0),
    _tau(0.0),
    dataPresent(false)
{
}

/****************************************************************************/

/** Destructor.
 */
ScalarVariant::~ScalarVariant() = default;

/****************************************************************************/

QtPdCom::Transmission getTrans(double sampleTime)
{
    if (sampleTime > 0.0) {
        return { std::chrono::duration<double>(sampleTime) };
    }
    if (sampleTime == 0.0) {
        return { QtPdCom::event_mode };
    }
    return { QtPdCom::poll_mode, -sampleTime };
}

/****************************************************************************/

void ScalarVariantPrivate::updateConnection()
{
  Q_Q(ScalarVariant);

    // FIXME test for scalar here
    if (process and process->isConnected()) {
        // we first allow only scalar elements as selector
        QStringList pathElements = path.split('#',
#if QT_VERSION > 0x050f00
                Qt::SkipEmptyParts
#else
                QString::SkipEmptyParts
#endif
                );

        if (pathElements.count() > 1) {
            bool ok;
            int index = pathElements.at(1).toInt(&ok);
            if (!ok) {
                qCritical() << "Only integer as path selector allowed "
                    "currently! Not registering the variable " << path;
                return;
            }
            PdCom::ScalarSelector selector({index});
#ifdef PD_DEBUG_SCALARVARIANT
            qDebug() << "setvariable path: " << pathElements.at(0)
                << ", index " << index
                << ", period: " << period;
#endif

            q->setVariable(process, pathElements.at(0), selector,
                           getTrans(period), _scale, _offset, _tau);
        } else {
            //PdCom::Selector all;
#ifdef PD_DEBUG_SCALARVARIANT
            qDebug() << "setvariable path " << path
                << ", period " << period;

#endif
            q->setVariable(process, path, {}, getTrans(period), _scale, _offset, _tau);
        }
    }
}

/****************************************************************************/

void ScalarVariant::setProcess(QtPdCom::Process *value){
  Q_D(ScalarVariant);

  //  qDebug() << "set Process" << value;
  if(value != d->process) {
    if(d->process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(d->process,0,0,0);
    }
    if(value) {
      d->process = value;
      QObject::connect(d->process, SIGNAL(processConnected()), this, SLOT(processConnected()));
      QObject::connect(d->process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(d->process, SIGNAL(error()), this, SLOT(processError()));
      //updateConnection();
    }
    //emit processChanged();
  }
}

/****************************************************************************/

void ScalarVariant::processConnected()
{
  Q_D(ScalarVariant);
  d->updateConnection();
}

/****************************************************************************/

void ScalarVariant::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void ScalarVariant::processError()
{
  clearData();
}

/****************************************************************************/

QVariant ScalarVariant::getConnection() const
{
  const Q_D(ScalarVariant);
  QVariantMap e;

  e["path"] = d->path;
  e["period"] = d->period;
  e["offset"] = d->_offset;
  e["scale"] = d->_scale;
  e["tau"] = d->_tau;
  return QVariant::fromValue(e);
}

/****************************************************************************/

void ScalarVariant::setConnection(QVariant value){
  Q_D(ScalarVariant);

  if (value.canConvert<QVariantMap>()) {
    QVariantMap connection = value.toMap();

    if(connection.contains("process")) {
      QtPdCom::Process *process = connection["process"].value<QtPdCom::Process *>();
      setProcess(process);
    } else { //try default process
      setProcess(QtPdCom::Process::getDefaultProcess());
    }

    connection.contains("path")?
      d->path = connection["path"].toString():
      d->path = "";

    d->period = 0;
    //"sampleTime"? backward compatibility
    connection.contains("sampleTime")?
      d->period = connection["sampleTime"].toDouble():d->period;

    connection.contains("period")?
      d->period = connection["period"].toDouble():d->period;

    connection.contains("offset")?
      d->_offset = connection["offset"].toDouble():
      d->_offset = 0;

    connection.contains("scale")?
      d->_scale = connection["scale"].toDouble():
      d->_scale = 1.0;

    connection.contains("tau")?
      d->_tau = connection["tau"].toDouble():
      d->_tau = 0.0;

    d->updateConnection();
    emit connectionUpdated();
  } else {
    qCritical() << "connection has to be a map";
  }
}

/****************************************************************************/

void ScalarVariant::setPath(QString value){
  Q_D(ScalarVariant);

   if(value != d->path) {
    d->path = value;
    d->period = 0;
    d->_offset = 0.0;
    d->_scale = 1.0;
    d->_tau = 0.0;
    //the design choice is, that setting only the path is for
    //event transmitted variables from the defaultProcess
    setProcess(QtPdCom::Process::getDefaultProcess());
    d->updateConnection();
    emit pathChanged(d->path);
  }
}

/****************************************************************************/

void ScalarVariant::clearData()
{
    Q_D(ScalarVariant);

    d->value = 0;
    d->dataPresent = false;
    emit dataPresentChanged(d->dataPresent);
    emit valueChanged(d->value);
}

/****************************************************************************/

void ScalarVariant::setValue(QVariant value)
{
    Q_D(ScalarVariant);

    if (not d->dataPresent or getVariable().empty()) {
        return;
    }

    //nothing to write
    if (value == d->value) {
      return;
    }

    switch (getVariable().getTypeInfo().type) {
        case PdCom::TypeInfo::boolean_T:
        case PdCom::TypeInfo::uint8_T:
        case PdCom::TypeInfo::uint16_T:
        case PdCom::TypeInfo::uint32_T:
        case PdCom::TypeInfo::uint64_T:
            if (value.canConvert<uint64_t>()) {
                writeValue((uint64_t) value.toULongLong());
            } else {
            qWarning() << "Variant datatype can't be converted to ULongLong";
            }
            break;
        case PdCom::TypeInfo::int8_T:
        case PdCom::TypeInfo::int16_T:
        case PdCom::TypeInfo::int32_T:
        case PdCom::TypeInfo::int64_T:
            if (value.canConvert<int64_t>()) {
                writeValue((int64_t) value.toLongLong());
            } else {
                qWarning() << "Variant datatype can't be converted to LongLong";
            }
            break;
        case PdCom::TypeInfo::single_T:
        case PdCom::TypeInfo::double_T:
            if(value.canConvert<double>()) {
                writeValue(value.toDouble());
            } else {
                qWarning() << "Variant datatype can't be converted to double";
            }
            break;
        default:
            qWarning() << "unknown datatype: can't write anything to process.";
            break;
    }
}

/****************************************************************************/

/** Increments the current #value and writes it to the process.
 *
 * This does \a not update #value directly.
 */
void ScalarVariant::inc()
{
    Q_D(ScalarVariant);

    writeValue(d->value.toInt() + 1);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void ScalarVariant::newValues(std::chrono::nanoseconds ts)
{
  Q_D(ScalarVariant);
  QVariant v;


#ifdef PD_DEBUG_SCALARVARIANT
  uint8_t c;
  d->copyData(c);
  qDebug() << "type " << getVariable().getTypeInfo().type << "value"  << c;
#endif

  switch (getVariable().getTypeInfo().type) {
  case PdCom::TypeInfo::boolean_T:
  case PdCom::TypeInfo::uint8_T:
  case PdCom::TypeInfo::uint16_T:
  case PdCom::TypeInfo::uint32_T:
  case PdCom::TypeInfo::uint64_T:
    {
      uint64_t uInt;
      d->copyData(uInt);
      v = (quint64)(uInt * d->_scale + d->_offset);
    }
    break;
  case PdCom::TypeInfo::int8_T:
  case PdCom::TypeInfo::int16_T:
  case PdCom::TypeInfo::int32_T:
  case PdCom::TypeInfo::int64_T:
    {
      int64_t sInt;
      d->copyData(sInt);
      //qDebug() << "sint " << sInt;
      v = (qint64)(sInt * d->_scale + d->_offset);
    }
    break;
  case PdCom::TypeInfo::single_T:
  case PdCom::TypeInfo::double_T:
    {
      double f;
      d->copyData(f);
      v = (double)(f * d->_scale + d->_offset);
    }
    break;
  default:
    qWarning() << "unknown datatype";
    v = (double)0.0;
    break;
  }

#ifdef PD_DEBUG_SCALARVARIANT
    QString path = getVariable().getPath().data();
    qDebug() << "new value " << v << " for " << path;
#endif

    if(!d->dataPresent) {
      d->dataPresent = true;
#ifdef PD_DEBUG_SCALARVARIANT
      qDebug() << "emit data present: " << d->dataPresent;
#endif
      emit dataPresentChanged(d->dataPresent);
      d->value = v;
      emit valueChanged(d->value);
    } else { //dataPresent == true

        double filterConstant = getFilterConstant();
        //design choice: filtering returns always a double variable
        if (filterConstant > 0.0) {
            v =
          filterConstant * (v.toDouble() - d->value.toDouble()) + d->value.toDouble();
        }

        if(d->value != v) {
            d->value = v;
            emit valueChanged(d->value);
        }
    }

    d->mTime = ts;
#ifdef PD_DEBUG_SCALARVARIANT
    qDebug() << "emit value updated: "
        << std::chrono::duration<double>(mTime).count();
#endif

    emit valueUpdated(getMTimeToDouble());
}

/****************************************************************************/

QString ScalarVariant::getPath() const
{
    const Q_D(ScalarVariant);
    return d->path;
}

Q_INVOKABLE bool ScalarVariant::getDataPresent() const
{
    const Q_D(ScalarVariant);
    return d->dataPresent;

}

std::chrono::nanoseconds ScalarVariant::getMTime() const
{
    const Q_D(ScalarVariant);
    return d->mTime;
}

double ScalarVariant::getMTimeToDouble() const
{
    const Q_D(ScalarVariant);
    return std::chrono::duration<double>(d->mTime).count();
}

bool ScalarVariant::hasData() const
{
    const Q_D(ScalarVariant);
    return d->dataPresent;
}

QtPdCom::Process *ScalarVariant::getProcess() const
{
    const Q_D(ScalarVariant);
    return d->process;
}


QVariant ScalarVariant::getValue() const
{
    const Q_D(ScalarVariant);
    return d->value;
}
