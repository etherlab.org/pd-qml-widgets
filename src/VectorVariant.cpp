/*****************************************************************************
 *
 * Copyright (C) 2018       Wilhelm Hagemeister <hm@igh.de>
 *               2021-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <PdQmlWidgets2/VectorVariant.h>
using PdQmlWidgets::VectorVariant;

#include <QtPdCom1/Transmission.h>

#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

#include <QtDebug>

/****************************************************************************/

// FIXME defined in ScalarVariant, but this file will be removed anyway?!
QtPdCom::Transmission getTrans(double sampleTime);

/****************************************************************************/

class PdQmlWidgets::VectorVariant::Impl
{
    friend class VectorVariant;

    public:
        Impl(VectorVariant *parent):
            parent(parent),
            scale(1.0),
            offset(0.0),
            path(""),
            period(0.0),
            pollOnce(false),
            polledOnce(false),
            dataPresent(false)
        {
        }

        double scaleToUser(double value)
        {
            return value * scale + offset;
        }

        double scaleToProcess(double value) const
        {
            if (scale) {
                return (value - offset) / scale;
            }
            else {
                return 0.0;
            }
        }

    private:
        VectorVariant *parent;
        QList<QVariant> items; /**< Current values. */
        double scale;
        double offset;
        QString path;
        double period;
        std::chrono::nanoseconds mTime; /**< Modification Time. */
        bool pollOnce;
        bool polledOnce;
        bool dataPresent; /**< process values are valid. */

        class Subscription;
        std::unique_ptr<Subscription> subscription;
};

/****************************************************************************/

class PdQmlWidgets::VectorVariant::Impl::Subscription:
    public PdCom::Subscriber, PdCom::Subscription
{
    friend class VectorVariant;

    public:
        Subscription(VectorVariant::Impl *impl, PdCom::Variable pv,
                const QtPdCom::Transmission &transmission):
            Subscriber(transmission.toPdCom()),
            PdCom::Subscription(*this, pv),
            impl(impl)
        {
        }

        Subscription(VectorVariant::Impl *impl, PdCom::Process *process,
                const std::string &path, const QtPdCom::Transmission &transmission):
            Subscriber(transmission.toPdCom()),
            PdCom::Subscription(*this, *process, path),
            impl(impl)
        {
        }

    private:
        VectorVariant::Impl * const impl;

        void stateChanged(const PdCom::Subscription &) override
        {
            if (getState() != PdCom::Subscription::State::Active) {
                impl->items.clear();
                impl->dataPresent = false;
                emit impl->parent->dataPresentChanged(impl->dataPresent);
                emit impl->parent->valueChanged();
            }

            if (getState() == PdCom::Subscription::State::Active) {
                if(impl->pollOnce && !impl->polledOnce) {
                    impl->subscription->poll(); // poll once to get initial value
                    impl->polledOnce = true;
                }

            }
            if (getState() == PdCom::Subscription::State::Invalid) {
                impl->polledOnce = false;
            }

        }

        void newValues(std::chrono::nanoseconds ts) override
        {
            auto pv(getVariable());
            auto nelem(pv.getSizeInfo().totalElements());

            impl->items.clear();

            switch (pv.getTypeInfo().type) {
                case PdCom::TypeInfo::boolean_T:
                case PdCom::TypeInfo::uint8_T:
                case PdCom::TypeInfo::uint16_T:
                case PdCom::TypeInfo::uint32_T:
                case PdCom::TypeInfo::uint64_T:
                    {
                        uint64_t uInt[nelem];
                        PdCom::details::copyData(uInt,
                                PdCom::details::TypeInfoTraits<uint64_t>
                                ::type_info.type,
                                getData(), pv.getTypeInfo().type, nelem);
                        for (size_t i = 0; i < nelem; i++) {
                            impl->items.append((qint64) uInt[i]);
                        }
                        impl->dataPresent = true;
                        emit impl->parent->dataPresentChanged(impl->dataPresent);

                        emit impl->parent->valueChanged();
                    }
                    break;

                case PdCom::TypeInfo::int8_T:
                case PdCom::TypeInfo::int16_T:
                case PdCom::TypeInfo::int32_T:
                case PdCom::TypeInfo::int64_T:
                    {
                        int64_t sInt[nelem];
                        PdCom::details::copyData(sInt,
                                PdCom::details::TypeInfoTraits<int64_t>
                                ::type_info.type,
                                getData(), pv.getTypeInfo().type, nelem);
                        for (size_t i = 0; i < nelem; i++) {
                            impl->items.append((qint64) sInt[i]);
                        }
                        impl->dataPresent = true;
                        emit impl->parent->dataPresentChanged(impl->dataPresent);

                        emit impl->parent->valueChanged();
                    }
                    break;

                case PdCom::TypeInfo::single_T:
                case PdCom::TypeInfo::double_T:
                    {
                        double d[nelem];
                        PdCom::details::copyData(d,
                                PdCom::details::TypeInfoTraits<double>
                                ::type_info.type,
                                getData(), pv.getTypeInfo().type, nelem);

                        for (size_t i = 0; i < nelem; i++) {
                            impl->items.append(impl->scaleToUser(d[i]));
                        }
                        impl->dataPresent = true;
                        emit impl->parent->dataPresentChanged(impl->dataPresent);

                        emit impl->parent->valueChanged();
                    }
                    break;

                default:
                    qWarning() << "unknown datatype";
                    break;
            }

            impl->mTime = ts;
            emit impl->parent->valueUpdated(ts);
        }
};

/****************************************************************************/

/** Constructor.
 */

VectorVariant::VectorVariant(QObject *parent):
    QObject(parent),
    process(0),
    impl(std::unique_ptr<VectorVariant::Impl>(new Impl(this)))
{
}

/****************************************************************************/

/** Destructor.
 */
VectorVariant::~VectorVariant()
{
    clearVariable();
}

/****************************************************************************/

void VectorVariant::setProcess(QtPdCom::Process *value){

  if(value != process) {
    if(process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(process,0,0,0);
    }
    if(value) {
      process = value;
      QObject::connect(process, SIGNAL(processConnected()), this, SLOT(processConnected()));
      QObject::connect(process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(process, SIGNAL(error()), this, SLOT(processError()));
    }
  }
}

/****************************************************************************/

void VectorVariant::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void VectorVariant::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void VectorVariant::processError()
{
  clearData();
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void VectorVariant::updateConnection()
{

  clearVariable();

  if (impl->path.isEmpty() or not process or not process->isConnected()) {
        return;
  }

  try {
        impl->subscription =
            std::unique_ptr<Impl::Subscription>(new Impl::Subscription(
                        impl.get(), process, impl->path.toLocal8Bit().constData(),
                        getTrans(impl->period)));
    } catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                " \"%1\" with sample time %2: %3")
            .arg(impl->path)
            .arg(impl->period)
            .arg(e.what());
        return;
    }

    if (impl->period == 0.0) {
      impl->pollOnce = true;
      //impl->subscription->poll(); // poll once to get initial value
    }

}

/****************************************************************************/

QVariant VectorVariant::getConnection(){
  QVariantMap e;

  e["path"] = impl->path;
  e["period"] = impl->period;
  e["offset"] = impl->offset;
  e["scale"] = impl->scale;
  return QVariant::fromValue(e);
}

/****************************************************************************/

void VectorVariant::setConnection(QVariant value){

  if (value.canConvert<QVariantMap>()) {
    QVariantMap connection = value.toMap();

    if(connection.contains("process")) {
      QtPdCom::Process *process = connection["process"].value<QtPdCom::Process *>();
      setProcess(process);
    } else { //try default process
      setProcess(QtPdCom::Process::getDefaultProcess());
    }

    connection.contains("path")?
      impl->path = connection["path"].toString():
      impl->path = "";

    impl->period = 0;
    //"sampleTime"? backward compatibility
    connection.contains("sampleTime")?
      impl->period = connection["sampleTime"].toDouble():impl->period;

    connection.contains("period")?
      impl->period = connection["period"].toDouble():impl->period;

    connection.contains("offset")?
      impl->offset = connection["offset"].toDouble():
      impl->offset = 0;

    connection.contains("scale")?
      impl->scale = connection["scale"].toDouble():
      impl->scale = 1.0;

    updateConnection();
    emit connectionUpdated();
  } else {
    qCritical() << "connection has to be a map";
  }
}

/****************************************************************************/

void VectorVariant::setPath(QString &value){
   if(value != impl->path) {
    impl->path = value;
    impl->period = 0;
    impl->offset = 0;
    impl->scale = 1;
    //the design choice is, that setting only the path is for
    //event transmitted variables from the defaultProcess
    setProcess(QtPdCom::Process::getDefaultProcess());
    updateConnection();
    emit pathChanged(impl->path);
  }
}

/****************************************************************************/

QString VectorVariant::getPath(){
  return impl->path;
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void VectorVariant::clearVariable()
{
    if (impl->subscription) {
        impl->subscription.reset();
        impl->dataPresent = false;
        emit impl->parent->dataPresentChanged(impl->dataPresent);

        clearData();
    }
}

/****************************************************************************/

/** Connected state.
 */
bool VectorVariant::hasVariable() const
{
    return impl->subscription
        and not impl->subscription->getVariable().empty();
}

/****************************************************************************/

void VectorVariant::clearData()
{
    if (impl->dataPresent) {
        impl->items.clear();
        impl->dataPresent = false;
        emit impl->parent->dataPresentChanged(impl->dataPresent);

        emit valueChanged();
    }
}

/****************************************************************************/

QVariant VectorVariant::getValue() const
{
    return impl->items;
}

/****************************************************************************/

void VectorVariant::setValue(const QVariant &value)
{
    if (!impl->subscription or impl->subscription->getVariable().empty()
            or !impl->dataPresent) {
        return;
    }

    PdCom::Variable pv(impl->subscription->getVariable());
    auto nelem(pv.getSizeInfo().totalElements());
    int cnt = nelem;
    bool isNumber{false};
    value.toDouble(&isNumber);
    bool isList = value.canConvert<QVariantList>(); // see assistant about
                                                    // canConvert() and
                                                    // convert()

    // a string can be stored in an uint8 array as well
    if (value.canConvert<QString>() and not isNumber and not isList) {
        if (pv.getTypeInfo().type == PdCom::TypeInfo::uint8_T) {
            QString s(value.toString());
            QByteArray data;
            do { /* the string with the trailing 0 must not be longer than the
                    array we need to chop the string and not the byteArray
                    because there can be an utf8 char at the end which is
                    longer than one byte */
                data = s.toUtf8();
                s.chop(1);
            } while (data.size() > cnt - 1);
            pv.setValue(data.data(),
                    PdCom::details::TypeInfoTraits<uint8_t>
                    ::type_info.type, data.size());

        }
        qWarning() << "type error: string is supplied to PdVector "
            << "but variable is not uint8";
        return; // can't be anything else
    }

    if (isList) {
        cnt = qMin(value.toList().count(), (int) nelem);
    }

    switch (pv.getTypeInfo().type) {
        case PdCom::TypeInfo::boolean_T:
        case PdCom::TypeInfo::uint8_T:
        case PdCom::TypeInfo::uint16_T:
        case PdCom::TypeInfo::uint32_T:
        case PdCom::TypeInfo::uint64_T:
            {
                uint64_t data[nelem];
                if (isList) {
                    for (int i = 0; i < cnt; i++) {
                        data[i] = impl->scaleToProcess(
                                value.toList().at(i).toULongLong(&isNumber));
                        if (not isNumber) {
                            goto out;
                        }
                    }
                }
                else { /* it might be a scalar, then set all items to the
                            same value */
                    for (int i = 0; i < cnt; i++) {
                        data[i] = impl->scaleToProcess(
                                value.toULongLong(&isNumber));
                        if (not isNumber) {
                            goto out;
                        }
                    }
                }
                pv.setValue(data,
                        PdCom::details::TypeInfoTraits<uint64_t>
                        ::type_info.type, cnt);
            }
            break;

        case PdCom::TypeInfo::int8_T:
        case PdCom::TypeInfo::int16_T:
        case PdCom::TypeInfo::int32_T:
        case PdCom::TypeInfo::int64_T:
            {
                int64_t data[nelem];
                if (isList) {
                    for (int i = 0; i < cnt; i++) {
                        data[i] = value.toList().at(i).toLongLong(&isNumber);
                        if (not isNumber) {
                            goto out;
                        }
                    }
                } else {
                    for(int i = 0; i < cnt; i++) {
                        data[i] = value.toLongLong(&isNumber);
                        if (not isNumber) {
                            goto out;
                        }
                    }
                }
                pv.setValue(data,
                        PdCom::details::TypeInfoTraits<int64_t>
                        ::type_info.type, cnt);
            }
            break;

        case PdCom::TypeInfo::single_T:
        case PdCom::TypeInfo::double_T:
            {
                double data[cnt];
                if (isList) {
                    for (int i = 0; i < cnt; i++) {
                        data[i] = impl->scaleToProcess(
                                value.toList().at(i).toDouble(&isNumber));
                        if (not isNumber) {
                            goto out;
                        }
                    }
                } else {
                    for(int i = 0; i < cnt; i++) {
                        data[i] = impl->scaleToProcess(
                                value.toDouble(&isNumber));
                        if (not isNumber) {
                            goto out;
                        }
                    }
                }
                pv.setValue(data,
                        PdCom::details::TypeInfoTraits<double>
                        ::type_info.type, cnt);
            }
            break;

        default:
            qWarning() << "unknown datatype";
            break;
    }
    return;

out:
    qWarning() << "type error: list is supplied to PdVector but "
        << "element are not numeric";
}

/****************************************************************************/

QString VectorVariant::getValueAsString() const
{
    if (impl->items.size() <= 0) {
        return QString();
    }

    QByteArray data;
    for (int i = 0; i < impl->items.size(); i++) {
        data.append(impl->items[i].toInt());
    }
    data.append('\0');
    return QString::fromUtf8(data);
}

/****************************************************************************/

void VectorVariant::setValueAsString(const QString &value)
{
    if (!impl->subscription or impl->subscription->getVariable().empty()
            or !impl->dataPresent) {
        return;
    }

    PdCom::Variable pv(impl->subscription->getVariable());
    auto nelem(pv.getSizeInfo().totalElements());
    int cnt = nelem;

    // a string can be stored in an uint8 array as well

    if (pv.getTypeInfo().type == PdCom::TypeInfo::uint8_T) {
        QString s = value;
        QByteArray data;
        do { /* the string with the trailing 0 must not be longer than the
                array.  We need to chop the string and not the byteArray
                because there can be an utf8 char at the end which is longer
                than one byte */
            data = s.toUtf8();
            s.chop(1);
        } while (data.size() > cnt - 1);
        data.append('\0');
        pv.setValue(data.data(),
                PdCom::details::TypeInfoTraits<uint8_t>
                ::type_info.type, data.size());
    }
}

/****************************************************************************/

std::chrono::nanoseconds VectorVariant::getMTime() const
{
    return impl->mTime;
}

/****************************************************************************/

double VectorVariant::getMTimeToDouble() const
{
    return std::chrono::duration<double>(impl->mTime).count();
}

/****************************************************************************/

bool VectorVariant::getDataPresent(){
  return impl->dataPresent;
}
