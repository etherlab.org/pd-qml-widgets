/*****************************************************************************
 *
 * Copyright (C) 2018-2021  Wilhelm Hagemeister<hm@igh.de>
 *               2018-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SCALARVARIANT_P_H
#define PD_SCALARVARIANT_P_H

#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/Process.h>

#include <QObject>
#include <QVariant>


namespace PdQmlWidgets {

class ScalarVariant;

/****************************************************************************/

/** Scalar Variant to be used in QML applications
    which is aware of the process to detect connections/or reconnect
 */

class ScalarVariantPrivate
{
    public:
        ScalarVariantPrivate(ScalarVariant *This);


    private:
        Q_DECLARE_PUBLIC(ScalarVariant);

        ScalarVariant * const q_ptr;
        QtPdCom::Process *process;
        QVariant value; /**< Current value. */
        QString path;
        double period;
        double _scale;
        double _offset;
        double _tau;
        std::chrono::nanoseconds mTime; /**< Modification Time of Current value. */
        bool dataPresent; /**< There is a process value to display. */
        void updateConnection(); /**< (re)connects to variable */

    template <typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, void>::type
    copyData(T &dest) const
    {
        const Q_Q(ScalarVariant);

        PdCom::details::copyData(
                &dest, PdCom::details::TypeInfoTraits<T>::type_info.type,
                q->getData(),
                q->getVariable().getTypeInfo().type, 1);
    }

};

/****************************************************************************/

} // namespace


#endif
