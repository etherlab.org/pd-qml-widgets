/****************************************************************************
**
** Copyright (C) 2021-2023 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** !!!!!!!!!!!! this is work in progress !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
**
** livesvg bietet die Möglichkeit svgs zu manipulieren
**
** 1. der Layer mit Namen: "dynamic" kann Komponenten enthalten, bei denen
**    Attribute aus QML heraus gesetzt werden können. Diese Komponenten werden
**    über die svg:id gefunden.
**    Beispiel: im svg     <g id="traverse">....</g> 
**
**              in qml 	    
      LiveSvg {
            id: svg
            .....
            Slider {
               onValueChanged: {
                   svg.changeAttribute("traverse","transform","translate(0,"+value+")");
   	           svg.update();
               }
            }
      }
** 
** 2. Im Layer "overlay" werde alle "rects" herausgesucht und alle
** "child" Componenten werden eingepasst, die im qml den "objectName" auf die id im svg-rect
** gesetzt haben.
**
**    Beispiel: im svg <rect
       id="pneumatic"
       width="1200"
       height="500"
       x="-3831.3479"
       y="1474.6614" />
                in qml

       LiveSvg {
            id:svg
            source: "qrc:/images/seitenansicht.svg"
            PdGauge {
 	        id:dbvGauge
	        objectName:"pneumatic"
            }
       }

**
** TODO: die absolute Position eines "rects" ergibt sich durch die Kette aller
** Transformationen im SVG. Bisher ist nur die "translation" implementiert!
**
** Auch wichtig: qt.svg unterstützt nur das tiny svg format Version: 1.2.
** d.h. nicht alle svg-Elemente werden unterstützt
**
**
****************************************************************************/

#include <QPainter>
#include <QDebug>
#include <QRectF>
#include <QtMath>
#include <QSizeF>
#include <QQuickItem>

#include <PdQmlWidgets2/LiveSvg.h>
#include "LiveSvg_p.h"
#include <QtDebug>

/* ---------------------------------------------------------------------------------
 */

using PdQmlWidgets::LiveSvg;
using PdQmlWidgets::LiveSvgPrivate;

LiveSvg::LiveSvg(QQuickItem *parent):
    QQuickPaintedItem(parent),
    d_ptr(new LiveSvgPrivate(this))
{
    //  this->setImplicitWidth(200);
    // this->setImplicitHeight(200);
}

/* ---------------------------------------------------------------------------------
 */

LiveSvg::~LiveSvg() = default;

/* ---------------------------------------------------------------------------------
 */

LiveSvgPrivate::LiveSvgPrivate(LiveSvg *This):
    m_svgdoc("svg"),
    viewBox(0, 0, 0, 0),
    rendererBounds(0,0,0,0),
    source(""),
    empty(true),
    invert(false),
    backgroundPixmapDirty(true),
    q_ptr(This)
{
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::clearSource()
{
    Q_D(LiveSvg);
    d->overlayElements.clear();
    d->backgroundPixmapDirty = true;
}

void LiveSvg::setInvert(bool value)
{
    Q_D(LiveSvg);
    if (value != d->invert) {
        d->invert = value;
	d->backgroundPixmapDirty = true;
        emit invertedChanged();
        update(QRect(0, 0, this->width(), this->height()));
    }
}
/* ---------------------------------------------------------------------------------
 */

void LiveSvg::setSource(const QString &s)
{
    Q_D(LiveSvg);
    // read svg-file (aka xml) in a DomDocument

    // to avoid a inconsistency between qml and c++ ressource file locators
    QString source(s);

    source = source.replace("qrc:", ":");

    if (d->source == source) {
        return;
    }

    d->source = source;

    d->empty = true;
    clearSource();

    QFile file(source);
    if (!file.open(QIODevice::ReadOnly)) {
        emit sourceChanged();
        qDebug() << "no file";
        return;
    }

    if (!d->m_svgdoc.setContent(&file)) {
        file.close();
        qDebug() << "svg could not be read";
        emit sourceChanged();
        return;
    }

    //set the dynamicDoc first to the same content
    d->m_svgDynamicDoc.setContent(d->m_svgdoc.toString());

    //and now delete the layer dynamic in the orginal doc
    {
	QDomElement elem = d->findLayer("dynamic",d->m_svgdoc.documentElement());
	if(!elem.isNull()) {
	    elem.parentNode().removeChild(elem);
	}
    }
    
     //and only keep the layer dynamic in svgDynamicDoc
     //we can't just cut the dynamic layer out of the svgDoc because
     //then global definitions in the svgDoc get lost!
    {
	QList<QDomElement>groups;
	QDomElement elem;
	d->findElementsWithAttribute(d->m_svgDynamicDoc.documentElement(),
				     "inkscape:groupmode",groups);
	foreach(elem,groups) {
	    if(elem.attribute("inkscape:groupmode") == "layer") {
		if(elem.attribute("inkscape:label") != "dynamic") {
		    elem.parentNode().removeChild(elem);
		}
	    }
	}
    }
    // get all recs in the Layer overlay
    d->getOverlayRects(d->m_svgdoc.documentElement());
    d->empty = false;
    d->backgroundPixmapDirty = true;
    emit sourceChanged();
    //d->findElementById(d->m_svgdoc.documentElement(),"bla");
}


/* ---------------------------------------------------------------------------------
 */

// Im SVG sollten sich mehrere Layer befinden. Nur der Layer, der mit msr
// markiert ist, wird dynamisch aktualisiert

// Im Layer Overlay befinden sich rects, die als Platzhalter für
// qml-Komponenten verwendet werden das Layer selber wird nicht gezeichnet

QDomElement
LiveSvgPrivate::findLayer(const QString &layerName, const QDomElement &parent)
{
    QList<QDomElement> groups;
    QDomElement elem;
    findElementsWithAttribute(parent, "inkscape:groupmode", groups);
    foreach (elem, groups) {
        if (elem.attribute("inkscape:groupmode") == "layer") {
            if (elem.attribute("inkscape:label") == layerName) {
                return elem;
            }
        }
    }
    return QDomElement();
}

/* ---------------------------------------------------------------------------------
 */

/* TODO we need to react to all transformations, right now only
   translations are taken into account!!
   Also the parser could be made with regular expressions....

   The parser is in:
   /vol/opt/qt/5.12.6/Src/qtsvg/src/svg/qsvghandler.cpp
   Function: static QMatrix parseTransformationMatrix(const QStringRef &value)

   !!!!
   Look at QMatrix QSvgRenderer::matrixForElement(const QString &id) const
   !!!! that should do as well and better ;-)
*/

void LiveSvgPrivate::getTransformations(const QDomNode &elem, QPointF &offset)
{
    if (!elem.parentNode().isNull()) {
        QString transform =
                elem.parentNode().toElement().attribute("transform");

        // FIXME where are all the other transformations: shear, scale,
        // matrix, rotate,....
        if (transform.contains("translate", Qt::CaseInsensitive)) {
            QStringList tList = transform.replace('"', "")
                                        .replace("translate", "")
                                        .replace('(', "")
                                        .replace(')', "")
                                        .split(',');  // FIXME regex?
            if (tList.size() == 2) {
                offset += QPointF(tList[0].toDouble(), tList[1].toDouble());
            }
        }

        getTransformations(elem.parentNode(), offset);
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::getOverlayRects(const QDomElement &parent)
{
    // find the layer which name is "overlay"
    QDomElement overlay = findLayer("overlay", parent);

    // get all the rects with id, x,y,width and height
    if (!overlay.isNull()) {
        // qDebug() <<  "overlay found";
        QDomNodeList rects = overlay.elementsByTagName("rect");
        for (int i = 0; i < rects.length(); i++) {
            QDomNamedNodeMap attr = rects.at(i).toElement().attributes();
            QVariantMap e;

            // get all translation offsets
            // FIXME take all transformations into account
            QPointF offset = QPointF(0, 0);
            getTransformations(rects.at(i), offset);
            // put into the map of the rect
            // we hope that in svg there will never be a "ox" and "oy"
            e["ox"] = offset.x();
            e["oy"] = offset.y();

            for (int j = 0; j < attr.count(); ++j) {
                QDomAttr attribute = attr.item(j).toAttr();
                e[attribute.name()] = attribute.value();
            }

            overlayElements.append(e);  // global in class
        }
        // qDebug() << "entries" <<  overlayElements;
        // qDebug() << "entries-------------------------";
    }
}

/* ---------------------------------------------------------------------------------
 */

QQuickItem *LiveSvgPrivate::findChildItem(QQuickItem *parent, const QString &name)
{
    QList<QQuickItem *> children = parent->childItems();
    foreach (QQuickItem *item, children) {
        if (item->objectName() == name) {
            return item;
        }
        // call recursively and browse through the whole scene graph of this
        // object
        if (QQuickItem *r = findChildItem(item, name))
            return r;
    }
    return NULL;
}

/* ---------------------------------------------------------------------------------
 */
/* ox and oy are scaled all ready ! */
void LiveSvgPrivate::scaleQmlChildren(
        double ox,
        double oy,
        double sx,
        double sy,
        QSvgRenderer &renderer)
{
    Q_Q(LiveSvg);

    /* QObject::findChild() does not work with repeater because in QML there
       is a real parent and a visual parent
       https://stackoverflow.com/questions/21623375/qml-repeater-parentness/22556528
       so we implemented a findChildItem method!
    */
    // qDebug() << "viewbox offset" << ox << ", " << oy;
    for (int i = 0; i < overlayElements.count(); i++) {
        QVariantMap e = overlayElements[i].toMap();
        // qDebug() << "search " << e["id"].toString();

        QQuickItem *item = findChildItem(q, e["id"].toString());
        if (item) {
            // qDebug() << "found ";
            /*
            qDebug() << "id: " << e["id"] << " dx: " << e["x"].toDouble() << "
            dy: " << e["y"].toDouble(); qDebug() << "id: " << e["id"] << " w:
            " << e["width"].toDouble() << " h: " << e["height"].toDouble();
            qDebug() << "id: " << e["id"] << " ox: " << e["ox"].toDouble() <<
            " oy: " << e["oy"].toDouble();

            qDebug() << "id: " << e["id"] << " sx: " << sx << " sy: " << sy;
            */
            /*
            item->setProperty("x",ox+(e["x"].toDouble() + e["ox"].toDouble())*
            sx); item->setProperty("y",oy+(e["y"].toDouble() +
            e["oy"].toDouble())* sy);
            item->setProperty("width",e["width"].toDouble() * sx);
            item->setProperty("height",e["height"].toDouble() * sy);
            */
            /*
            double x = ox+(e["x"].toDouble() + e["ox"].toDouble())* sx;
            double y = oy+(e["y"].toDouble() + e["oy"].toDouble())* sy;
            double w = e["width"].toDouble() * sx;
            double h = e["height"].toDouble() * sy;
            qDebug() << "center x"  << x + w/2 << "center y" << y + h/2;
            */


            QRectF bounds = renderer.boundsOnElement(e["id"].toString());
            QPointF center = bounds.center();

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
            QTransform m = renderer.transformForElement(e["id"].toString());
#else
            QMatrix m = renderer.matrixForElement(e["id"].toString());
            // qDebug() << "bounds :" << bounds << " center: "  << center <<
            // "matrix: " << m;
#endif

            // apply the transformation matrix (FIXME only translation)
            center += QPointF(m.dx(), m.dy());

            // FIXME we do the rotation outside or later

            double w = e["width"].toDouble();
            double h = e["height"].toDouble();
            item->setProperty("x", ox + (center.x() - w / 2) * sx);
            item->setProperty("y", oy + (center.y() - h / 2) * sy);
            item->setProperty("width", w * sx);
            item->setProperty("height", h * sy);
        }
    }
}

// TEST
void LiveSvgPrivate::getTransform(QSvgRenderer &renderer)
{
    for (int i = 0; i < overlayElements.count(); i++) {
        QVariantMap e = overlayElements[i].toMap();
        QString eid = e["id"].toString();

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
        QTransform m = renderer.transformForElement(eid);
#else
        QMatrix m = renderer.matrixForElement(eid);
#endif

        qDebug() << "id: " << eid
                 << " matrix: " << m;
        qDebug() << "id: " << eid
                 << "bounds: " << renderer.boundsOnElement(eid);
    }
}


/* ---------------------------------------------------------------------------------
 */

void LiveSvg::updateBackgroundPixmap()
{
    Q_D(LiveSvg);
    QPainter painter;

    d->backgroundPixmap = QPixmap(QSize(this->width(), this->height()));
    d->backgroundPixmap.fill(Qt::transparent);


    painter.begin(&d->backgroundPixmap);
    painter.setRenderHints(QPainter::Antialiasing);

    if (d->invert) {  // FIXME very dirty hack.....
        QString doc = d->m_svgdoc.toString();
        doc = doc.replace("#000000", "#ffffff");
        d->m_renderer.load(doc.toUtf8());
    }
    else {
        d->m_renderer.load(d->m_svgdoc.toByteArray());
    }
    d->viewBox = d->m_renderer.viewBoxF();
    emit viewBoxChanged();

    // calc the bounds for preserveAspectFit (which is most likely wanted?!)
    // image is centered
    double scale =
            qMin(this->width() / d->viewBox.width(),
                 this->height() / d->viewBox.height());

    d->rendererBounds =
            QRectF((this->width() - d->viewBox.width() * scale) / 2,
                   (this->height() - d->viewBox.height() * scale) / 2,
                   d->viewBox.width() * scale,
                   d->viewBox.height() * scale);


//        qDebug() << "svg viewbox" <<  d->viewBox;
        qDebug() << "svg bounds" <<  d->rendererBounds;


    d->m_renderer.render(&painter, d->rendererBounds);
    painter.end();
    if (d->viewBox.width() > 0 && d->viewBox.height() > 0) {
        d->scaleQmlChildren(
                d->rendererBounds.x() - d->viewBox.x() * scale,
                d->rendererBounds.y() - d->viewBox.y() * scale,
                scale,
                scale,
                d->m_renderer);
        // DBG getTransform(m_renderer);
        emit scaleChanged(QSizeF(scale, scale));
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::paint(QPainter *painter)
{
    Q_D(LiveSvg);
    // qDebug() << "paint size" << this->width() << " x " << this->height();
    if (!d->empty) {
	if(d->backgroundPixmap.size() != QSize(width(),height())
	   || d->backgroundPixmapDirty) {
	    updateBackgroundPixmap();
	    d->backgroundPixmapDirty = false;
	}
    }
    painter->drawPixmap(0, 0, d->backgroundPixmap);
    
    //now the dynamic part
    d->m_renderer.load(d->m_svgDynamicDoc.toByteArray());
    d->m_renderer.render(painter,d->rendererBounds);
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::changeAttribute(QString id,QString name, QString value) {
    Q_D(LiveSvg);
    d->changeAttribute(id,name,value);
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::changeAttribute(const QString &id,const QString &name,const QString &value) {
    
    QDomElement elem = findElementById(m_svgDynamicDoc.documentElement(),
				       id); //FIXME this is expensive
    //we might maintain a list where only the elements are prefetched
    //which are intended to be altered
    if(!elem.isNull()) {
	elem.setAttribute(name,value);
    }
}


/* ---------------------------------------------------------------------------------
 */
/* thanks to
https://stackoverflow.com/questions/17836558/can-i-find-all-xml-nodes-with-a-given-attribute-in-a-qdomdocument
*/

void LiveSvgPrivate::findElementsWithAttribute(
        const QDomElement &elem,
        const QString &attr,
        QList<QDomElement> &foundElements)
{
    if (elem.attributes().contains(attr))
        foundElements.append(elem);

    QDomElement child = elem.firstChildElement();
    while (!child.isNull()) {
        findElementsWithAttribute(child, attr, foundElements);
        child = child.nextSiblingElement();
    }
}

/* ---------------------------------------------------------------------------------
 */

QDomElement LiveSvgPrivate::findElementById(
        const QDomElement &elem,
        const QString &idValue)
{
    if (elem.attributes().contains("id")) {
	if (elem.attribute("id") == idValue) {
	    return elem;
	}
    }

    QDomElement child = elem.firstChildElement();
    while (!child.isNull()) {
	QDomElement elem = findElementById(child,idValue);
	if(!elem.isNull())
	    return elem;
        child = child.nextSiblingElement();
    }
    return QDomElement();
}

/* -----for debugging
 * ------------------------------------------------------------- */

void LiveSvgPrivate::printElements(QList<QDomElement> elements)
{
    QDomElement elem;
    qDebug() << "count: " << elements.count();
    foreach (elem, elements) {
        qDebug() << "Tagname" << elem.tagName();
        // and list all attributes
        printAttributes(elem);
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvgPrivate::printAttributes(QDomElement elem)
{
    QDomNamedNodeMap var = elem.attributes();
    for (int i = 0; i < var.count(); ++i) {
        QDomAttr attribute = var.item(i).toAttr();
        qDebug() << "Attr: " << attribute.name() << ": " << attribute.value();
    }
}


QVariant LiveSvg::getOverlayElements() const
{
    const Q_D(LiveSvg);
    return d->overlayElements;
}

QString LiveSvg::getSource() const
{
    const Q_D(LiveSvg);
    return d->source;
}

bool LiveSvg::getInvert() const
{
    const Q_D(LiveSvg);
    return d->invert;
}

QRectF LiveSvg::getViewBox() const
{
    const Q_D(LiveSvg);
    return d->viewBox;
}
