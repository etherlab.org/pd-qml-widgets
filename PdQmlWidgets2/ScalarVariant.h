/*****************************************************************************
 *
 * Copyright (C) 2018-2021  Wilhelm Hagemeister<hm@igh.de>
 *               2018-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SCALARVARIANT_H
#define PD_SCALARVARIANT_H

#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/Process.h>

#include <QScopedPointer>
#include <QObject>
#include <QVariant>


namespace PdQmlWidgets {

class ScalarVariantPrivate;

/****************************************************************************/

/** Scalar Variant to be used in QML applications
    which is aware of the process to detect connections/or reconnect
 */

class Q_DECL_EXPORT ScalarVariant:
  public QObject, public QtPdCom::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    /** if one want's to update the variable connection values in one call
       supply a map with {"path":path(string),
                          "period":sampleTime(double),
                          "offset":offset(double),
			  "scale":scale(double),
			  "tau":tau(double) PT1-Filter 0: if no filter
                          ....
                         }
     */
    Q_PROPERTY(QVariant connection READ getConnection WRITE setConnection NOTIFY connectionUpdated)

    /* indicates that the process is connected and data is transfered */
    Q_PROPERTY(bool connected READ getDataPresent NOTIFY dataPresentChanged)

    Q_PROPERTY(QVariant value READ getValue WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(double mtime READ getMTimeToDouble NOTIFY valueUpdated)
    public:
        ScalarVariant(QObject *parent = nullptr);
        virtual ~ScalarVariant();

        QString getPath() const;
        Q_INVOKABLE bool getDataPresent() const;
        QVariant getConnection() const;

        void clearData(); // pure-virtual from ScalarSubscriber
        Q_INVOKABLE bool hasData() const;
        QtPdCom::Process *getProcess() const;

        void setPath(QString);
        void setConnection(QVariant);

        void setProcess(QtPdCom::Process *);
        QVariant getValue() const;
        Q_INVOKABLE void setValue(QVariant);
        std::chrono::nanoseconds getMTime() const;
        double getMTimeToDouble() const;
        Q_INVOKABLE void inc();

    private:
        Q_DECLARE_PRIVATE(ScalarVariant);
        QScopedPointer<ScalarVariantPrivate> const d_ptr;

        Q_DISABLE_COPY(ScalarVariant)

    // pure-virtual from ScalarSubscriber
    void newValues(std::chrono::nanoseconds);



    private slots:
        void processConnected();
    void processDisconnected();
    void processError();

    signals:
        void valueChanged(QVariant); /**< Emitted, when the value changes, or the
                                       variable is disconnected. */
        void valueUpdated(double); /**< Emitted also when value does not change but
                                     got an update from the msr process */
        void pathChanged(QString &);
        void dataPresentChanged(bool);
        void connectionUpdated();
};

/****************************************************************************/

} // namespace


#endif
