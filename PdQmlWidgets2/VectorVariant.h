/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the PdQmlWidgets library.
 *
 * The PdQmlWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PdQmlWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdQmlWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_VECTORVARIANT_H
#define PD_VECTORVARIANT_H

#include <QtPdCom1/Process.h>

#include <QObject>
#include <QVariant>

#include <chrono>

#if QT_VERSION < 0x050000
#define Q_NULLPTR NULL
#endif

namespace PdQmlWidgets {

/****************************************************************************/

/** Vector Variant to be used in QML applications.
 */
class Q_DECL_EXPORT VectorVariant:
    public QObject
{
    Q_OBJECT

    /** Assign path: this selects the default process and event transmission.
     */
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)

    /** if one wants to update the variable connection values in one call
      supply a map with {"path":path,
      "period":sampleTime
      ....
      }
     */
    Q_PROPERTY(QVariant connection READ getConnection WRITE setConnection
            NOTIFY connectionUpdated)

    /** Indicates that the process is connected and data is transfered.
     */
    Q_PROPERTY(bool connected READ getDataPresent NOTIFY dataPresentChanged)

    /** Process value.
     */
    Q_PROPERTY(QVariant value READ getValue WRITE setValue
            NOTIFY valueChanged)

    /** Interpret the elements of an process variable array as string.
     */
    Q_PROPERTY(QString text READ getValueAsString WRITE setValueAsString
            NOTIFY valueChanged)

    /** last modification time of process variable
     */
    Q_PROPERTY(QVariant mtime READ getMTimeToDouble
            NOTIFY valueUpdated)

    public:
        VectorVariant(QObject * = Q_NULLPTR);
        ~VectorVariant();

        void clearVariable();
        bool hasVariable() const;
        void clearData();

        QVariant getValue() const;
        Q_INVOKABLE void setValue(const QVariant &);
        QString getValueAsString() const;
        Q_INVOKABLE void setValueAsString(const QString &);
        bool getDataPresent();

        std::chrono::nanoseconds getMTime() const;
        double getMTimeToDouble() const;

        QString getPath();
        void setPath(QString &);
        QVariant getConnection();
        void setConnection(QVariant);
        void updateConnection(); /**< (re)connects to variable */
        void setProcess(QtPdCom::Process *);

    private slots:
        void processConnected();
        void processDisconnected();
        void processError();

    signals:
        void valueChanged(); /**< Emitted, when the value changes, or the
                               variable is disconnected. */
        void valueUpdated(std::chrono::nanoseconds mtime); /**< Emitted also
                                                             when value does
                                                             not change but
                                                             got an update
                                                             from the msr
                                                             process */
        void pathChanged(QString &);
        void connectionUpdated();
        void dataPresentChanged(bool);

    private:
        class Q_DECL_HIDDEN Impl;
        QtPdCom::Process *process;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

} // namespace


#endif
