/****************************************************************************
**
**
****************************************************************************/

#ifndef PD_LIVESVG_H
#define PD_LIVESVG_H

#include <QScopedPointer>
#include <QString>
#include <QSize>
#include <QPixmap>
#include <QSizeF>
#include <QPointF>
#include <QPainter>
#include <QtQuick/QQuickPaintedItem>

namespace PdQmlWidgets {
class LiveSvgPrivate;

class Q_DECL_EXPORT LiveSvg: public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QRectF viewBox READ getViewBox NOTIFY viewBoxChanged)
    Q_PROPERTY(QString source READ getSource WRITE setSource NOTIFY
                       sourceChanged)  // Image
    Q_PROPERTY(
            bool invert READ getInvert WRITE setInvert NOTIFY invertedChanged)
  public:
    LiveSvg(QQuickItem *parent = 0);
    ~LiveSvg();

    void paint(QPainter *painter);
    Q_INVOKABLE QVariant getOverlayElements() const;
    Q_INVOKABLE QString getSource() const;
    
    Q_INVOKABLE void changeAttribute(QString, QString, QString);
    bool getInvert() const;
    void setInvert(bool);
    void setSource(const QString &s);
    void clearSource();

    QRectF getViewBox() const;
    // public slots:

  signals:
    void scaleChanged(QSizeF scale);
    void viewBoxChanged();
    void sourceChanged();
    void invertedChanged();

  private:
    void updateBackgroundPixmap();
    Q_DECLARE_PRIVATE(LiveSvg);

    QScopedPointer<LiveSvgPrivate> const d_ptr;

    Q_DISABLE_COPY(LiveSvg)
};

/****************************************************************************/

}  // namespace PdQmlWidgets

#endif
