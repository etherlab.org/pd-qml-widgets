------------------------------------------------------------------------------
vim: tw=78 syntax=markdown
------------------------------------------------------------------------------

# General Information

This is the Pd QML Widgets library, a process-data aware widget library for
Qt/QML from the EtherLab project. See
https://gitlab.com/etherlab.org/pd-qml-widgets

------------------------------------------------------------------------------

# License

The Pd QML Widgets library is licensed unter the terms and conditions of the
GNU Lesser General Public License (LGPL), version 3 or (at your option) any
later version.

------------------------------------------------------------------------------

# Prerequisites

   - Qt5
   - PdCom 5 (see http://etherlab.org/en/pdcom)
   - QtPdCom 1

------------------------------------------------------------------------------

# Building and Installing

## cmake
The build process is handled by cmake. To build and install, call:


```
mkdir build && cd build
cmake ..
make install
```

To change the installation path,
call cmake with `-DCMAKE_INSTALL_PREFIX=/my/custom/prefix`.
If PdCom5 and/or QtPdCom1 is installed at a non-standard location,
use `-DCMAKE_PREFIX_PATH=/pdcom/install/prefix`.
Multiple paths can be joined by semicolon (`;`).
You can even use this option to point CMake to a build directory of
PdCom5 and/or QtPdCom1, this adds the possibility to use a version
of PdCom5 and/or QtPdCom1 which is not installed at all.

To import this library into your own application,
add the following to your `CMakeLists.txt`:
```cmake
find_package(PdQmlWidgets2 REQUIRED)
target_link_libraries(your_application PUBLIC
   EtherLab::PdQmlWidgets2
)
```
All dependencies are imported and linked automatically.
The `CMAKE_PREFIX_PATH` argument works here, too.

------------------------------------------------------------------------------

# Note regarding QML Files

The qml files are now compiled into PdQmlWidgets.
To use the compiled-in version, call `Pd::setQmlImportPathToResourceFile`
at the begin of your application.
This loads the resource file and adds it to the QML import path.
There is no need to add `de_igh_pdQmlWidgets.qrc` to your application anymore.

If you wish to use the qml files from the filesystem,
set `QML2_IMPORT_PATH` to the corresponding directory
and don't call `Pd::setQmlImportPathToResourceFile`.

Have fun!

------------------------------------------------------------------------------
