/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* 
*
* TODO: Documentation
*
*
****************************************************************************/


import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.3

/** Digital display and touch edit but in contrast to PdTouchEdit no 
    realtime process variable!
 */

Label {
    id: control
    property alias decimals: dialog.decimals
    property alias suffix: dialog.suffix
    property alias lowerLimit: dialog.lowerLimit
    property alias upperLimit: dialog.upperLimit
    property alias title: dialog.title
    
    property alias dialog: dialog
    
    readonly property alias value: control._value
    /**
       value always gets updated, if initialValue is modified
    */
    property double initialValue: 0.0

    /**
       this is to make value readonly
    */
    property double _value: 0.0

    onInitialValueChanged: _value = initialValue
    
    //text: _value.toFixed(decimals)+suffix
    text: _value.toLocaleString(Qt.locale(),'f',decimals) + suffix

    /**
       this signal is emited if the value is modified by the Dialog.
       It is not emited if the value is modified from other msr connections
    */

    signal accepted()

    property alias touchEditDialog: dialog

    TouchEditDialog {
        id:dialog
        onAccepted: {
            control._value = value
      	    control.accepted()
	}
    }

    SystemPalette { id: palette }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:palette.base
        border.color: parent.enabled ? palette.dark:palette.light

        MouseArea {
            anchors.fill: parent
            onClicked: {         
                dialog.value = control._value
                dialog.open() 
            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
