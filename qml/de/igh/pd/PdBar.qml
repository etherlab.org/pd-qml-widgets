/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** 
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Simple Bar without scale but with a label inside showing the value
 */

Control {
    id:control
    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1,
     *         "transmission":Pd.periodic
     *     }
     * }
     * @endcode
     */
    property alias variable: scalar
    property alias connection: scalar.connection
    
    /** type:string
     * Suffix is appended to the value without space!
     */
    property var suffix:""

    /** type:string
     * Prefix is appended to the value without space!
     */
    property var prefix:""

    /** type:int
     * Number of decimals digitals for the label
     */
    property var decimals:2
    
    /** type:bool
     * Draw bars originating from zero otherwise from the minium. 
     */
    property bool startZero:true

    property int orientation:Qt.Horizontal
    /** type:bool
     * Show the value label if true.
     */
    property alias labelVisible:label.visible

    /** type:int
     * pixes size for label; applies only for vertical orientation!
     */
    property int fontPixelSize:12 
    
    /** type:doube
     * This property holds the starting value of the control. Default: 0
     */
    property double from: 0
    
    /** type:double
     * This property holds the end value of the control. Default: 100
     */
     property double to: 100

    /** type:var
     * color of bar
     */
    property color color: control.palette.dark

    /** type:var
     * color of background
     */
    property color backgroundColor: control.palette.button

    /** type:var
     * color of border
     */
    property color borderColor: control.palette.dark

    /** type:var
     * width of border
     */
    property int borderWidth: 0

    property alias label: label

    property int radius: 0
    /** type:double
     * The position is expressed as a fraction of the value, 
     * in the range 0.0 - 1.0.
     */
    readonly property double visualPosition: 1.0 * (Math.min(Math.max(value,from),to) - from)/(to - from)

    /** type:double
     * depending on startZero: start the bar at the "0" position or the "from" position
     */
    readonly property double visualStart: (from < 0 && to > 0 && startZero)?1.0 * ( 0 - from)/(to - from):0
    
    /** type: var
     * Raw Process value
     */
    readonly property alias value: scalar.value

    enabled:scalar.connected

    background: Rectangle {
	color: control.backgroundColor
	border.color: control.borderColor
	border.width: control.borderWidth
	implicitWidth: orientation==Qt.Horizontal?100:20
	implicitHeight: orientation==Qt.Horizontal?20:100
	radius: control.radius
    }
    
    contentItem: Item { //contentItem is of the size of the control !
	//we use the rotation here, because the y-axis of gui components starts at the top of the screen
	//but the y-axis of the bar should start at the bottom
	//the easiest way is to rotate the contentItem in Vertical mode
	rotation: control.orientation == Qt.Vertical?180:0 //fixme functioniert das immer?
	Rectangle { //Rectangle must be able to be size to any size (within the control) !
	    color: control.color
            function scaleOrigin(l) {
		return control.visualStart * l
	    }
	    
            function scale(l) {
		    return control.visualPosition * l
	    }
	    
            x: {
		if(control.orientation == Qt.Horizontal) {
		    let w = parent.width - 2 * control.borderWidth;
		    return control.borderWidth + Math.min(scaleOrigin(w),scale(w));
		} else {  //vertical
		    return control.borderWidth
		}
            }

            width: {
		if(control.orientation == Qt.Horizontal) {
		    let w = parent.width - 2 * control.borderWidth;
		    return Math.abs(scaleOrigin(w) - scale(w));
		} else {
		    return parent.width - 2 * control.borderWidth
		}
	    }
	    
	    y: {
		if(control.orientation == Qt.Horizontal) {
		    return control.borderWidth
		} else {
		    let h = parent.height - 2 * control.borderWidth;
		    return control.borderWidth + Math.min(scaleOrigin(h),scale(h));
		}
	    }
            height: {
		if(control.orientation == Qt.Horizontal) {
		    return parent.height - 2 * control.borderWidth
		} else {
		    let h = parent.height - 2 * control.borderWidth;
		    return Math.abs(scaleOrigin(h) - scale(h));
		}
	    }
	}
	Label {
	    id:label
            anchors.centerIn:parent
	    rotation: control.orientation == Qt.Vertical?180:0

            text:control.prefix + scalar.value.toLocaleString(Qt.locale(),'f',control.decimals) + control.suffix
	    font.family: "Roboto" //monospace font für digitals
            font.pixelSize: {
		if(control.orientation == Qt.Horizontal) {
		    return control.height*8/10
		} else {
		    return control.fontPixelSize
		}
	    }
	}
    }
    PdScalar {
	id:scalar
    }
} //Progressbar 
