import QtQuick 2.7         
import QtQuick.Controls 1.4

import de.igh.pd 2.0

CheckBox {
    id: checkbox
    property alias variable:scalar
    // deprecated --------------
    property alias process: scalar.process
    property alias path: scalar.path
    // deprecated --------------
    property bool invert:false
    enabled:scalar.connected	
    PdScalar {
        id:scalar
        value: (checkbox.checked != invert)
    }
    checked: ((scalar.value != 0) != invert)
}
