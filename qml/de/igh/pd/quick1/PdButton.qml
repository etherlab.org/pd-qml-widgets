import QtQuick 2.7         
import QtQuick.Controls 1.4

import de.igh.pd 2.0

Button {
    id: control
    property alias variable:scalar
    // deprecated --------------
    property alias process: scalar.process
    property alias path: scalar.path
    // deprecated --------------
    property bool invert:false
    checkable:true
    property bool event:false //increments the value by one when pressed
                              //works only when checkable is false
    onEventChanged: {
	if(event) {
	    checkable = false
	}
    }
    
    PdScalar { id:scalar }
    
    // reactions when button is checkable
    checked: ((scalar.value != 0) != invert) && checkable
    onClicked: {
	if(checkable) {
	    scalar.value = (control.checked != invert)
	}
	if(event) {
	    scalar.inc()
	}
    }
    
    //reactions when button is not checkable
    /*
    onPressed: {
	if(!checkable && !event) {
	    scalar.value = !invert
	}
    }
    onReleased: {
	if(!checkable && !event) {
	    scalar.value = invert
	}	
    }
*/
}
