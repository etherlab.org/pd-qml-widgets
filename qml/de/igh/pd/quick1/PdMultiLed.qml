/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** 
** Same Component as ../PdMultiLed; different style
**
**
****************************************************************************/

import QtQuick 2.7         

import de.igh.pd 2.0

Item {
    id: indicator
    property int radius:8
    property alias variable:scalar
    property alias connection:scalar.connection

    property alias path:scalar.path

    
    property var led:_led /** allows to tune properties of the led */
    
    property var map:{} /* Example:
			   {
			    const map = new Map()
			    map.set(0,{"color":"red","blink":false});
			    map.set(1,{"color":"green","blink":true});
			    map.set(2,{"color":"green","blink":false});
			    map.set(3,{"color":"#FF9800","blink":true});
			    map.set(4,{"color":"blue","blink":false});
			    return map;
			    } */
    property color color: "red"
    property bool active: true
    property bool value: scalar.value
    implicitHeight:radius*2
    implicitWidth:radius*2
    opacity:scalar.connected?1:0.3
    Rectangle {
	id:_led
	anchors.fill:parent
	radius: Math.max(width/2,height/2)
	color:indicator.active?indicator.color:Qt.darker(indicator.color,2)
	border.width:0
	antialiasing:true
    }

    //im Gegensatz zu active welches beim blinken wegfällt
    PdScalar {
        id:scalar
        onValueChanged: {
	    if ((map != undefined) && (map.get(value) != undefined)) {
		if(map.get(value).color != undefined) {
		    indicator.color = map.get(value).color;
		    indicator.active = true;
		    if (map.get(value).blink != undefined) {
			timer.running = map.get(value).blink;
		    } else {
			timer.running = false
		    }
		    return
		}
	    }
	    indicator.active = false
	    timer.running = false
	}
    }
    Timer {
        id:timer
        interval: 500;
        repeat: true;
        onTriggered: {
            indicator.active = (!indicator.active)
        }
    }
}
