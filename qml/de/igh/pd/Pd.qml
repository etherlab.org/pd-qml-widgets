pragma Singleton

/****************************************************************************
**
** Copyright (C) 2023 Wilhelm Hagemeister
** Contact: hm@igh.de
**
**
** Constants and Definitions for PdQmlWidget
**
****************************************************************************/

import QtQml 2.2

QtObject {
    /**
       use virtual numpad instead of TouchEditDialog
     */
    property bool useNumpad: false;
}
