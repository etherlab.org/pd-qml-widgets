pragma Singleton

/****************************************************************************
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** https://ethanschoonover.com/solarized/
**
****************************************************************************/


import QtQuick 2.5
import QtQuick.Controls.Material 2.4
import QtQml 2.2

 QtObject {
     readonly property color base03:  "#002b36"
     readonly property color base02:  "#073642"
     readonly property color base01:  "#586e75"
     readonly property color base00:  "#657b83"
     readonly property color base0:   "#839496"
     readonly property color base1:   "#93a1a1"
     readonly property color base2:   "#eee8d5"
     readonly property color base3:   "#fdf6e3"
     readonly property color yellow:  "#b58900"
     readonly property color orange:  "#cb4b16"
     readonly property color red:     "#dc322f"
     readonly property color magenta: "#d33682"
     readonly property color violet:  "#6c71c4"
     readonly property color blue:    "#268bd2"
     readonly property color cyan:    "#2aa198"
     readonly property color green:   "#859900"
 }
