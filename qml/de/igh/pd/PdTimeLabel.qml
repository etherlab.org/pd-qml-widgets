/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** TimeLabel: not checked against QtPdWidgets 
**
** TODO: documentation
**
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

import "Sprintf.js" as Str

/** Show absolute or relative Time/Date from process value in seconds
    
 */

Label {
    id: control

    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1, 
     *         "transmission":Pd.Periodic|Pd.Event|Pd.Poll
     *     }
     * }
     * @endcode
     * 
     * Defaults are:
     * period: 0
     * scale: 1
     * offset: 0
     * transmission: Pd.Event
     *
     */
    property alias variable: scalar
    property alias connection: scalar.connection
    
    /**type:var
     * convenience connection to the process path
     * see @link PdCheckBox @endlink
     * use with caution: default transmission is "event" meaning
     * every change of the signal triggers a transmission
     */
    property alias path: scalar.path

    /** type:string
     * Suffix is appended to the value without space!
     */
    property string suffix: ""

    /** type:string
     * Prefix is prepended to the value without space!
     */
    property string prefix: ""

    property var format: Locale.LongFormat 

    /** type: var
     * Raw Process value
     */
    property alias value: scalar.value

    enum Mode {  
        Time, 
        DateLocal, 
        DateTimeLocal,
        DateUTC,
        DateTimeUTC
    }

    //change this if mode is fully implemented
    property int mode: PdTimeLabel.Mode.Time

    PdScalar {
        id:scalar
        onValueChanged:{
            switch(control.mode) {
            case PdTimeLabel.Mode.Time:
                var sec = value % 60;
                var min = ((value % 3600)-sec)/60;
                var h = (value - sec - min*60)/3600;
                control.text = prefix + Str.sprintf("%02d:%02d:%02d",h,min,sec) + suffix;
                break;
	    case PdTimeLabel.Mode.DateLocal:
		var date = new Date(value * 1000);
		control.text = prefix + date.toLocaleDateString(Qt.locale("de_DE"),format) + suffix;
		break;
	    case PdTimeLabel.Mode.DateTimeLocal:
		var date = new Date(value * 1000);
		control.text = prefix + date.toLocaleString(Qt.locale("de_DE"),format) + suffix;
		break;
	    case PdTimeLabel.Mode.DateTimeUTC:
		var date = new Date(value * 1000);
		//var dateUTC = new Date(date.getTime() - date.getTimezoneOffset()*60000);
		control.text = prefix + date.toUTCString() + suffix; //dateUTC.toLocaleString(Qt.locale("de_DE"),format);
		break;
            default:
                control.text = "not implemented yet";
                break;
            }
        }
    }
}
