if(NOT QML_DIR)
    set(QML_DIR "${CMAKE_INSTALL_LIBDIR}/qt5/qml")
endif()
set(QML_PATH "de/igh/pd.2")

install(FILES
    de_igh_pdQmlWidgets.pri
    de_igh_pdQmlWidgets.qrc
    FontAwesome.otf
    qmldir
    Sprintf.js
    LoginDialog.qml
    Pd.qml
    PdBar.qml
    PdCheckBox.qml
    PdComboBox.qml
    PdDigital.qml
    PdVectorDigital.qml
    PdLabel.qml
    PdText.qml
    PdImage.qml
    PdLed.qml
    PdMultiLed.qml
    PdPushButton.qml
    PdToolButton.qml
    PdButton.qml
    PdSwitch.qml
    PdStatusIndicator.qml
    PdSlider.qml
    PdTimeLabel.qml
    PdVectorLed.qml
    PdTouchEdit.qml
    TouchEdit.qml
    TouchEdit_2_1.qml
    TouchEdit_2_3.qml
    TouchEditDialog.qml
    TouchEditDialog_2_2.qml
    TouchEditDialog_2_3.qml
    TouchEditNumpad.qml
DESTINATION "${QML_DIR}/${QML_PATH}")

install(DIRECTORY
    +material
    quick1
    style
DESTINATION "${QML_DIR}/${QML_PATH}")
