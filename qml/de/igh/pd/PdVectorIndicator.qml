import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4

import de.igh.pd 1.0

Item {
	id:control	
    property alias process: vector.process
    property alias path: vector.path
    property alias scale: vector.scale
    property alias sampleTime:vector.sampleTime
    property alias value:vector.value
    property bool invert:false
    property bool blinking:false
    property alias interval: timer.interval
    readonly property real size: Math.min(width, height)

    property var colorHash:[]   //take the color from the colorHash if that fits to the value
    property color color: "red" //otherwise take color
    property color offColor: Qt.darker(control.color,1.8) //rgba(0.13, 0.13, 0.13)	
    implicitWidth: 34
    implicitHeight: 34

    //from Status Indicator
    readonly property real outerRecessPercentage: 0.22
    
    readonly property color baseColor: control.color
    //~from Status Indicator


    PdVector {
        id:vector
        onValueChanged: {
        }
    }

    Timer {
        id:timer
        interval: 500;
        repeat: true;
        running: control.blinking
        property bool toggle:false;
        onTriggered: {
            toggle = !toggle
            canvas.requestPaint()
        }
    }

    Canvas {
        id: canvas
        anchors.fill: parent

        Connections {
            target: control
            onValueChanged: canvas.requestPaint()
            onColorChanged: canvas.requestPaint()
        }

        onPaint: {

            var cnt = vector.value.length;
            // console.log("cnt"+cnt)

            var centerX =  parent.size/2;
            var centerY =  parent.size/2;
            var radius = parent.size/2 * (1.0 - outerRecessPercentage);

            var ctx = getContext("2d");
            ctx.reset();

            // Draw the semi-transparent background.
            ctx.beginPath();
            var gradient = ctx.createLinearGradient(width / 2, 0, width / 2, height * 0.75);
            gradient.addColorStop(0.0, Qt.rgba(0, 0, 0, 0.25));
            gradient.addColorStop(1.0,Qt.rgba(0.74, 0.74, 0.74, 0.25));
            
            ctx.fillStyle = gradient;
            ctx.ellipse(0, 0,width,height);
            ctx.fill();

            // Draw the arcs
            
            for(var i = 0;i<cnt;i++) {
                var drawColor = control.offColor;
                //test Hash first
                if(colorHash[vector.value[i]] != undefined) {
                    // console.log("hash"+colorHash[vector.value[i]])
                    if(!control.blinking || (control.blinking && timer.toggle)) {
                        drawColor = colorHash[vector.value[i]];
                        gradient = ctx.createRadialGradient(width / 2, height / 2, width * 0.25, 
                        width / 2, height / 2, width * 0.25);
                        gradient.addColorStop(0.14, Qt.lighter(drawColor, 1.6));
                        gradient.addColorStop(1.0,drawColor);
                        //ctx.fillStyle = gradient; //FIXME this does not work (any more?)
                        ctx.fillStyle = drawColor;
                    } else {
                        drawColor = Qt.darker(colorHash[vector.value[i]],1.8);
                        ctx.fillStyle = drawColor;
                    }
                } else { //"normal" behaviour
                    if((vector.value[i] != 0) != invert && (!control.blinking || (control.blinking && timer.toggle))) {
                        // console.log("hit: "+vector.value[i]+"|"+invert+"|"+control.blinking+"|"+timer.toggle+"|"+control.baseColor)
                        gradient = ctx.createRadialGradient(width / 2, height / 2, width * 0.25, 
                        width / 2, height / 2, width * 0.25);
                        gradient.addColorStop(0.14, Qt.lighter(control.baseColor, 1.6));
                        gradient.addColorStop(1.0,control.baseColor);
                        ctx.fillStyle = gradient; //FIXME this does not work (any more?)
                        ctx.fillStyle = control.baseColor;
                    } else {
                        ctx.fillStyle = control.offColor;
                    }
                }
                var startAngle = Math.PI * 2 / cnt * i - Math.PI / 2;
                var endAngle = startAngle + Math.PI * 2 / cnt;
                ctx.lineWidth = parent.size / 20;
                ctx.beginPath();
                ctx.moveTo(centerX,centerY); //Mittelpunkt
                ctx.arc(centerX,centerY, radius,startAngle, endAngle);
                ctx.closePath();
                ctx.fill();
            }
        }
    }
}
