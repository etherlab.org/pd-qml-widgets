/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** 
** Same Component as PdStatusIndicator but other Interface to pdScalar
**
**
****************************************************************************/
import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Extras 1.4

import de.igh.pd 2.0

StatusIndicator {
    id: indicator
    
    property alias variable:scalar
    property alias connection:scalar.connection
    
    property bool invert:false
    
    property bool blinking:false
    
    readonly property bool value: ((scalar.value != 0) != invert)
    
    opacity:scalar.connected?1:0.3
    //im Gegensatz zu active welches beim blinken wegfällt
    PdScalar {
        id:scalar
        onValueChanged: {
            if(value != invert) {
                if(blinking) {
                    timer.running = true
                } else {
                    indicator.active = true
                }
            } else {
                timer.running = false
                indicator.active = false
            }
        }
    }
    Timer {
        id:timer
        interval: 500;
        repeat: true;
        onTriggered: {
            indicator.active = (!indicator.active)
        }
    }
}
