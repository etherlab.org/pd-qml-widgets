/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* 
*
* TouchEdit with virtual Keyboard / alternative to TouchEditDialog
*
*
****************************************************************************/

import QtQuick 2.9           
import QtQuick.Layouts 1.3   
import QtQuick.Controls 2.3
import QtQml 2.12

import "Sprintf.js" as Str

/**
 * Dialog for editing numeric inputs via touch
 */

Dialog {
    id:dialog
    anchors.centerIn: Overlay.overlay
    modal:true
    clip: true
    property double value:0
    property string valueString: ""
    property int decimals:2
    property string suffix:""
    property double lowerLimit:-Number.MAX_VALUE
    property double upperLimit:Number.MAX_VALUE
    property int btnWidth: 120
    property int btnHeight: 70

    property var updateValue: undefined //callback for updating
    
    FontLoader {
        source: "FontAwesome.otf"
    }

    header: Item {
        visible:(dialog.title.length > 0)? true:false
	implicitHeight:titleLabel.height*1.5
	Label {
	    anchors.centerIn:parent
	    topPadding:10
	    horizontalAlignment:Text.AlignHCenter
	    verticalAlignment:Text.AlignVCenter
            id:titleLabel
	    text:dialog.title
	    font.bold:true
	}

	RoundButton { 
	    anchors.right:parent.right
	    anchors.top:parent.top
	    anchors.rightMargin:10
	    anchors.topMargin:10
	    text:"c"
	    onClicked: { Pd.useNumpad = false;
		       	 ToolTip.show("Die Eingabemethode wird beim nächsten Öffnen dieses Dialoges umgestellt.",3000)
		       }
	}
	
    }

    
     onDecimalsChanged: { 
        decimals = Math.max(decimals,0);
    }

    // -----------------------------------------------------------------
    function updateValueString() {
	valueString = value.toLocaleString(Qt.locale(),'f',decimals);
	valueLabel.text = valueString + suffix
    }

    function validate(s) {
	
	valueLabel.enabled = true

	let gs = Qt.locale().groupSeparator;

	valueString = s.split(gs).join(""); //delete all groupSeparator
	
	//console.log("valueLabel" + valueString+ "gs " + gs)
	
	if (valueString.length > 0) {
	    //Limit first
	    //place new groupSeparators (we do that only on the integerpart)
	    let parts = valueString.split(Qt.locale().decimalPoint);

	    let gsStr = "";

	    let k = 0;
	    //console.log("parts" + JSON.stringify(parts))
	    
	    for(let i = parts[0].length-1; i>=0; i--) {
		gsStr = parts[0][i] + gsStr;
		k++;
		if(k == 3 && (i>1 || (parts[0][0] != '-' && i>0))) {
		    gsStr = gs + gsStr;
		    k = 0;
		}
	    }
	    parts[0] = gsStr;
		
	    if (parts.length > 1) {
		valueString = parts[0] + Qt.locale().decimalPoint + parts[1];
	    } else {
		valueString = parts[0];
	    }
	    
	}
	valueLabel.text = valueString + suffix
    }

    ColumnLayout {
	spacing:10
        Label {
	    id:valueLabel
	    //text: valueString + suffix
	    font.pixelSize:30
            horizontalAlignment: Qt.AlignHCenter
	    verticalAlignment: Qt.AlignVCenter
	    Layout.fillWidth: true
	}
	Label {
	    id: errorLabel
	    color:"red"
            horizontalAlignment: Qt.AlignHCenter
	    verticalAlignment: Qt.AlignVCenter
	    Layout.fillWidth: true
	    visible: false
	}
	
        //Button Grid
        GridLayout {
            columns:4
            columnSpacing:10
            rowSpacing:10
	    Repeater {
		model:["1","2","3",Qt.locale().decimalPoint,"4","5","6","-","7","8","9","Clear"]
		Button {
                    text: modelData
                    font.pixelSize: 20
		    font.bold:true
                    implicitHeight: dialog.btnHeight
                    implicitWidth: dialog.btnWidth
                    autoRepeat:true
                    onClicked: {
			if(text === "Clear")
			{ validate("");
			  errorLabel.visible = false
			}
			else
			    validate(valueString + text)  //only chars
		    }
		    enabled: {
			switch (text) {
			case Qt.locale().decimalPoint:
			    return !(valueString.indexOf(Qt.locale().decimalPoint) > -1 ||
				valueString.length == 0 || decimals <= 0)
			    break
			case "-": //only at beginning
			    return valueString.length == 0
			    break
			case "": return false
			    break
			case "Clear":
			    return valueString.length > 0
			    break
			default:
			    let idp = valueString.indexOf(Qt.locale().decimalPoint); //limit the number of digits after
			    return !(idp > -1 && valueString.length - idp > dialog.decimals)
			    break
			}
		    }
		}
	    }
		
	    Button {
                id:back
                font.family: "FontAwesome"
                font.pixelSize: 20
		font.bold: true
                text: "\uf060"
                    implicitHeight:dialog.btnHeight
                    implicitWidth:dialog.btnWidth
                autoRepeat:true
                onClicked: {
		    validate(valueString.slice(0,-1));
		    errorLabel.visible = false
		}

		enabled: valueString.length > 0
            }
	    Button {
                id:zero
                font.pixelSize: 20
                text: "0"
		font.bold: true
		implicitHeight:dialog.btnHeight
                implicitWidth:dialog.btnWidth
                autoRepeat:true
                onClicked: validate(valueString + text);
		enabled: {
		    let idp = valueString.indexOf(Qt.locale().decimalPoint); //limit the number of digits after
		    return !(idp > -1 && valueString.length - idp > dialog.decimals)
		}
            }
	    Button {
                id:accept
		font.family: "FontAwesome"
                font.pixelSize: 20
		font.bold: true
                text: "\uf00c"
		implicitHeight:dialog.btnHeight
                implicitWidth:dialog.btnWidth
                autoRepeat:true
		enabled: valueString.length > 1 || (valueString.length > 0 && valueString[0] != '-')
                onClicked:{
		    if (updateValue != undefined && valueString.length > 0) {
			let v = Number.fromLocaleString(Qt.locale(),valueString);
			if (v > upperLimit) {
			    errorLabel.text = "Der Wert überschreitet das obere Limit von: "+
				upperLimit.toLocaleString(Qt.locale(),'f',decimals) + suffix;
			    errorLabel.visible = true
			} else {
			    if (v < lowerLimit) {
				errorLabel.text = "Der Wert unterschreitet das untere Limit von: "+
				    lowerLimit.toLocaleString(Qt.locale(),'f',decimals) + suffix;
				errorLabel.visible = true
			    } else {
				updateValue(v)
				
				dialog.close()
			    }
			}
		    }
		}
	    }
	    Button {
                font.pixelSize: 20
                text: "Cancel"
		font.bold: true
		implicitHeight:dialog.btnHeight
                implicitWidth:dialog.btnWidth
                autoRepeat:true
                onClicked: dialog.close()
            }
	}
    }

    onOpened: {
	console.log("TouchEditNumpad is deprecated! Update your import to: de.igh.pd 2.3.")
	//value has been set
	updateValueString()
	valueLabel.enabled = false
	//reset
	valueString = ""
	errorLabel.visible = false

    }
} //Dialog
