/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: difference with pdWidgets:
** no "timeDisplay" and no "Base"
** 
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Base functionality for digital displays.
 * 
 */

Label {
    id:control

    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1, 
     *         "transmission":Pd.Periodic|Pd.Event|Pd.Poll
     *     }
     * }
     * @endcode
     * 
     * Defaults are:
     * period: 0
     * scale: 1
     * offset: 0
     * transmission: Pd.Event
     *
     */
    property alias variable: scalar
    property alias connection: scalar.connection

    /**type:var
     * convenience connection to the process path
     * see @link PdCheckBox @endlink
     * use with caution: default transmission is "event" meaning
     * every change of the signal triggers a transmission
     */

    property alias path: scalar.path

    /** type:string
     * Suffix is appended to the value without space!
     */
    property string suffix: ""

    /** type:string
     * Prefix is prepended to the value without space!
     */
    property string prefix: ""

    /** type:int
     * Number of decimals value is formated with.
     */
    property int decimals: 2

    /** type: var
     * Raw Process value
     */
    readonly property alias value: scalar.value

    /** type: bool
     * Connection state of the variable.
     * True: Data is updated from the control process.
     */
    property alias connected: scalar.connected

    /** type:bool
     * This signal is emitted when a mouse is hovered over the control. 
     * true: when the mouse moves over the control, false: otherwise
     */
    property alias hovered: ma.containsMouse

    /** type:bool
	* this shows a little up or down arrow behind the Value builded from the gradient of the
	* signal. The following heuristics is used: it is only necessary to show the gradient
	* if the signal change is smaller than the settings of "digital". Above that it can be seen
	* at the signal (label) itself. The threshold will be than one decay smaller:
	* thus: 10^(-digital-1)
     */
    property bool showTrend:false
    
    enabled: scalar.connected

    property string gradientIndicator:""

    
    PdScalar { id:scalar
	       property double prev: 0.0
	       property double gradientThreshold:Math.pow(10, -control.decimals-1)
	       
	       onValueUpdated: {
		   //console.log("v " + value + " p " + prev + " g " + gradientThreshold)
		   if(control.showTrend) {
		       if(value - prev > gradientThreshold) {
			   control.gradientIndicator="<sup><font size=2>▲</font></sup>";
			   //control.gradientIndicator="<sup>▲</sup>";
		       } else {
			   if(value - prev < -1.0 * gradientThreshold) {
			       control.gradientIndicator="<sub><font size=2>▼</font></sub>";
//			       control.gradientIndicator="<sub>▼</sub>";
			   } else {
			       control.gradientIndicator = '\u2004'
			   } /* if right alignment is used we need a space as
				wide as the arrows to avoid jumping */
		       }
		   } else {
		       control.gradientIndicator = "";
		   }
		   prev = value;
	       }
	     }

    font.family: "Roboto" //monospace font für digitals

    textFormat: Text.RichText
    text: (prefix + value.toLocaleString(Qt.locale(),'f',decimals) + suffix + gradientIndicator);

    //add a Mousearea to have the hovered event
    MouseArea {
            id: ma
            anchors.fill: parent
            hoverEnabled: true
    }

}
