/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: not in QtPdWidgets available
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Switch
 */

Switch {
    id: control
    
    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */

    property alias path: scalar.path

    /**type:bool
       Invert the function of the switch.
    */
    property bool invert:false

    enabled: scalar.connected

    PdScalar { id:scalar }
    checked: ((scalar.value != 0) != invert)
    onClicked: scalar.value = (control.checked != invert)
}
