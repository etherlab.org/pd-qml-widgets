/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** 
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

//FIXME: Slider does not react on enabled
//TODO: update value as slider moves

/** Slider
*/
Slider {
    id: control
    
      /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */

    property alias path: scalar.path

    enabled:scalar.connected

    PdScalar {
        id:scalar
        onValueChanged: {
            if(!control.pressed) { //do not refresh if control is pressed
                control.value = value
            }
        }
    }
    //only write the value at release
    onPressedChanged:{
        if(!pressed) {
            scalar.value = value
        }
    }
}
