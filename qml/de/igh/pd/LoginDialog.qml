import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtQml 2.2

Dialog {
    title: qsTr("Login...")
    id: dialog

    property QtObject loginManager

    parent: Overlay.overlay
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)

    standardButtons: Dialog.Ok | Dialog.Cancel
    modal: true
    contentWidth: col.implicitWidth
    contentHeight: col.implicitHeight

    property alias username:usernameInput.text
    closePolicy:Popup.NoAutoClose | Popup.CloseOnEscape

    GridLayout {
	rows:1
	rowSpacing:20
	columnSpacing:20
	id:col
	Label {
	    text:qsTr("Username:")
	}
	TextField {
	    id:usernameInput
	    Layout.alignment:Qt.AlignRight
        onAccepted: {
            dialog.accept()
        }
	}
	Item {
	    height:1
	    width:50
	    Layout.fillWidth:true
	}
	Label {
	    text:qsTr("Password:")
	}
	TextField {
	    id:passwordInput
        echoMode: TextInput.Password
	    Layout.alignment:Qt.AlignRight
        onAccepted: {
            dialog.accept()
        }
	}
    } // GridLayout
    onReset: {
		usernameInput.text = "";
		passwordInput.text = "";
    }

    onAccepted: {
        loginManager.setAuthName(usernameInput.text)
        loginManager.setPassword(passwordInput.text)
        passwordInput.text = ""
        loginManager.login()
    }
}
