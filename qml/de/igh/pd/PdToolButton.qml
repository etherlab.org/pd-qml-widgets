import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

//changes the appearence outline and filled from a pdvariable

ToolButton { 
    id:control
        
    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path:scalar.path
    
    property alias value: scalar.value
    
    property color backgroundColor: palette.button
    
    property color activeColor:palette.highlight
    
    property bool invert:false
    enabled: scalar.connected

    contentItem: Text {
        font: control.font
        text: control.text
        opacity: enabled ? 1.0 : 0.3
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        style: ((scalar.value != 0) != control.invert)? Text.Normal : Text.Outline
        color: ((scalar.value != 0) != control.invert)? control.activeColor : control.backgroundColor
    }

    PdScalar {
        id:scalar
    }
}
