/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: Same properties as pdWidgets-PushButton (Comments see src/PushButton.cpp)
**
****************************************************************************/


import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Push Button.
 *
 */

Button {
    id: control
    
    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path:scalar.path
    
    enum ButtonMode {
        PressRelease, /**< The #onValue is written when the button is
                         pressed down, the #offValue is written when
                         releasing it, respectively. */
        Event /**< An event is generated, based on #EventCondition
                 and #EventAction settings. */
    }
    
    enum EventCondition {
	OnClicked, /**< Event generated by QPushButton's clicked() signal
                      (button is pressed and released, while the cursor is
                      inside the button area. */
        OnPressed, /**< Event generated by QPushButton's pressed() signal
                      (button just pressed down). */
        OnReleased /**< Event generated by QPushButton's released() signal
                      (button released, regardless of cursor being inside
                      the button area). Consider using #OnClicked. */
    }

    enum EventAction {
            IncrementValue, /**< The value of the process variable is
                              incremented. */
            SetOnValue, /**< The #onValue is written to the process variable.
                         */
            ToggleValue /**< The #offValue is written to the process if the
                           last value was the #onValue. Otherwise the #onValue
                           is written to the process. */
    }


    /** The button behaviour. */
    property int buttonMode:PdPushButton.ButtonMode.PressRelease
    
    /** Event condition. */
    property int eventCondition:PdPushButton.EventCondition.OnClicked
    
    /** Action on an event. */
    property int eventAction:PdPushButton.EventAction.IncrementValue

    /**type:int
     * The value to be written, when the button is
     * pressed. */
    property int onValue:1
    
    /**type:int
     * The value to be written, when the button is
     * released. */
    property int offValue:0

    enabled: scalar.connected

    PdScalar {
	id:scalar
	onValueChanged: {
	    if (control.buttonMode == PdPushButton.ButtonMode.PressRelease &&
		control.checkable) {
		control.checked = (value == control.onValue);
	    }
	}
    }

    onButtonModeChanged: {
	if(buttonMode == PdPushButton.ButtonMode.Event) {
	    checkable = false
	}
    }

    onClicked: {
	console.log("click")
	if (buttonMode == PdPushButton.ButtonMode.Event &&
	    eventCondition == PdPushButton.EventCondition.OnClicked) {
            triggerEvent();
	}
	else if (buttonMode == PdPushButton.ButtonMode.PressRelease && checkable) {
            if (checked) {
		scalar.value = onValue
            }
            else {
		scalar.value = offValue
            }
	}
    }

    onPressed: {
	console.log("press")
	if (buttonMode == PdPushButton.ButtonMode.PressRelease && !checkable) {
	    console.log("write: "+ onValue)
	    scalar.value = onValue
	} else if (buttonMode == PdPushButton.ButtonMode.Event &&
		   eventCondition == PdPushButton.EventCondition.OnPressed) {
            triggerEvent();
	}
    }
    
    onReleased: {
	console.log("release")

	  if (buttonMode == PdPushButton.ButtonMode.PressRelease && !checkable) {
	    console.log("write: "+ offValue)
            scalar.value = offValue
        }
        else if (buttonMode == PdPushButton.ButtonMode.Event &&
		 eventCondition == PdPushButton.EventCondition.OnReleased) {
            triggerEvent();
        }
    }

    function triggerEvent() {
	switch (eventAction) {
        case PdPushButton.EventAction.IncrementValue:
            scalar.inc() 
            break;

        case PdPushButton.EventAction.SetOnValue:
            scalar.value = onValue;
            break;

        case PdPushButton.EventAction.ToggleValue:
            if (scalar.value == onValue) {
                scalar.value = offValue;
            }
            else {
                scalar.value = onValue;
            }
            break;
	}
    }
}
