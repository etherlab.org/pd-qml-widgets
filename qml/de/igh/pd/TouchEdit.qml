/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* 
*
* TODO: Documentation
*
*
****************************************************************************/


import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Digital display and touch edit but in contrast to PdTouchEdit no 
    realtime process variable!
 */

Label {
    id: control
    property alias decimals:dialog.decimals
    property alias suffix:dialog.suffix
    property alias lowerLimit:dialog.lowerLimit
    property alias upperLimit:dialog.upperLimit
    property double value:0.0
    property alias title: dialog.title
    //text: value.toFixed(decimals)+suffix
    text: value.toLocaleString(Qt.locale(),'f',decimals) + suffix
    
    /**
       this signal is emited if the value is modified by the Dialog.
       It is not emited if the value is modified from other msr connections
    */

    signal accepted()

    TouchEditDialog {
        id:dialog
        onAccepted: {
            control.value = value
      	    control.accepted()
	}
    }

    SystemPalette { id: palette }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:palette.base
        border.color: parent.enabled ? palette.dark:palette.light

        MouseArea {
            anchors.fill: parent
            onClicked: {         
                dialog.value = control.value
                dialog.open() 
            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
