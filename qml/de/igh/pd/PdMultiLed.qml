/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: similar component in QtPdWidgets 
**
** TODO: Documentation
**
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4

import de.igh.pd 2.0

StatusIndicator {
    id: indicator
    
    property alias variable:scalar
    property alias connection:scalar.connection

    property alias path:scalar.path
    
    property var map:{} /* Example:
			   {
			    const map = new Map()
			    map.set(0,{"color":"red","blink":false});
			    map.set(1,{"color":"green","blink":true});
			    map.set(2,{"color":"green","blink":false});
			    map.set(3,{"color":"#FF9800","blink":true});
			    map.set(4,{"color":"blue","blink":false});
			    return map;
			    } */
    
    opacity:scalar.connected?1:0.3
    //im Gegensatz zu active welches beim blinken wegfällt
    PdScalar {
        id:scalar
        onValueChanged: {
	    if ((map != undefined) && (map.get(value) != undefined)) {
		if(map.get(value).color != undefined) {
		    indicator.color = map.get(value).color;
		    indicator.active = true;
		    if (map.get(value).blink != undefined) {
			timer.running = map.get(value).blink;
		    } else {
			timer.running = false
		    }
		    return
		}
	    }
	    indicator.active = false
	    timer.running = false
	}
    }
    Timer {
        id:timer
        interval: 500;
        repeat: true;
        onTriggered: {
            indicator.active = (!indicator.active)
        }
    }
}
