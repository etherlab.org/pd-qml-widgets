/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: not in pdWidgets
** 
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Base functionality for digital displays.
 * Can show vectors and derived functions as mean, std, max, min.
 * See property mode
 */

Label {
    id:control

    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1, 
     *         "transmission":Pd.Periodic|Pd.Event|Pd.Poll
     *     }
     * }
     * @endcode
     * 
     * Defaults are:
     * period: 0
     * scale: 1
     * offset: 0
     * transmission: Pd.Event
     *
     */
    property alias variable: vector
    property alias connection: vector.connection

    property string suffix:""
    property int decimals:2
    property alias value: vector.value
    property alias connected:vector.connected
    enabled:vector.connected

    property int index: -1 //if index is positive only the value of that index
                           //is chown (regardless off the mode)
    property var indices : [] //if Mode = Selection only elements with indexes in this vector are shown
    
    enum Mode {  //for vector elements
        All,     //show all elements
        Limits,  //show the max and minimum
        Mean,    //show mean and std
        Set,     //show a set of distinct values (makes only sense for integers)
	Selection //shows the indices which are listed in the indices vector
    }
    property int mode: PdVectorDigital.Mode.All

    PdVector {
        id:vector
    }

    //Helper functions
    function mean(data){
        var sum = data.reduce(function(sum, value){
            return sum + value;
        }, 0);

        var avg = sum / data.length;
        return avg;
    }

    function std(values){
        var avg = mean(values);
        
        var squareDiffs = values.map(function(value){
            var diff = value - avg;
            var sqrDiff = diff * diff;
            return sqrDiff;
        });
        
        var avgSquareDiff = mean(squareDiffs);

        var stdDev = Math.sqrt(avgSquareDiff);
        return stdDev;
    }
    
    //http://jkorpela.fi/chars/spaces.html
    function norm(t) {
        if(t[0] != '-') {
            return '\u2004'+t;
        } else {
            return t;
        }
    }

    text:{
        var t = "";
        if(value.length == 1) { //scalar
            t = norm(value[0].toLocaleString(Qt.locale(),'f',control.decimals));
        } else  {
            if (index > -1) { //auch scalar
                t = norm(value[Math.min(index,value.length-1)].toLocaleString(Qt.locale(),'f',control.decimals));
            } else {                //vector
                switch(mode) {
                case PdVectorDigital.Mode.All: 
                    for(var i=0;i<value.length;i++) {
                        t+= norm(value[i].toLocaleString(Qt.locale(),'f',control.decimals));
                        if(i != value.length -1) {
                            t+=", ";
                        }
                    }
                    break;
                case PdVectorDigital.Mode.Selection:
		    for(var i=0;i<indices.length;i++) {
                        t+= norm(value[indices[i]].toLocaleString(Qt.locale(),'f',control.decimals));
                        if(i != indices.length -1) {
			    t+=", ";
                        }
		    }
                    break;
                case PdVectorDigital.Mode.Limits: 
                    t = norm(Math.min.apply(null,value).toLocaleString(Qt.locale(),'f',control.decimals))+' ... '+
			norm(Math.max.apply(null,value).toLocaleString(Qt.locale(),'f',control.decimals));
                    break;

                case PdVectorDigital.Mode.Mean:
                    t = "μ: " + norm(mean(value).toLocaleString(Qt.locale(),'f',control.decimals))+" (σ: "+
			norm(std(value).toLocaleString(Qt.locale(),'f',control.decimals))+")";
                    break;

                case PdVectorDigital.Mode.Set:
                    var set = [];
                    var hit;
                    //FIXME geht sicher performanter
                    t = "{";
                    for(var i=0;i<value.length;i++) {
                        hit = false;
                        for(var j=0;j<set.length;j++) {
                            if (value[i] == set[j]) {
                                hit = true;
                                break;
                            }
                        }

                        if(hit == false) {
                            set.push(value[i]);
                        }
                    }
                    set.sort(function(a, b){return a - b});
                    for(var i=0;i<set.length;i++) {
                        t+= norm(set[i].toLocaleString(Qt.locale(),'f',control.decimals));
                        if(i != set.length -1) {
                            t+=", ";
                        }
                    }
                    t+= "}";
                    break;
                }
            }
        }   					    
        t = t + control.suffix;
        return t;
    }
}
