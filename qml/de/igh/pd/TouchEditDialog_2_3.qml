/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021-2023 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* 
*
* TouchEdit with virtual Keyboard and classic TouchEditDialog
* one of the both components is instanciated and directy writing 
* to properties of the dialog
*
****************************************************************************/

import QtQuick 2.9           
import QtQuick.Layouts 1.3   
import QtQuick.Controls 2.3
import QtQml 2.12

import "Sprintf.js" as Str

Dialog {
    id:dialog
    //FIXME: there are application where Overlay.overlay can't be used
    //resolve this in later versions! Hm 2024-04-05
    anchors.centerIn: Overlay.overlay

    /* FIXME this is not always running; don't know why? Hm
    parent: ApplicationWindow.overlay
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)
    */

    modal:true
    property double value:0
    property int decimals:2
    property string suffix:""
    property double lowerLimit:-Number.MAX_VALUE
    property double upperLimit:Number.MAX_VALUE

    property var updateValue: undefined //callback for updating 
    focus: true

    
    header: Item {
        //visible:(dialog.title.length > 0)? true:false
	implicitHeight:titleLabel.height*1.5
	Label {
	    anchors.centerIn:parent
	    topPadding:10
	    horizontalAlignment:Text.AlignHCenter
	    verticalAlignment:Text.AlignVCenter
            id:titleLabel
	    text:dialog.title
	    font.bold:true
	}
	RoundButton {
	    anchors.right:parent.right
	    anchors.top:parent.top
	    anchors.rightMargin:10
	    anchors.topMargin:10
	    text: !Pd.useNumpad?"\uf11c":"\u002b"
            font.family: "FontAwesome"
            font.pixelSize: 20
	    flat: true
	    onClicked:  Pd.useNumpad = !Pd.useNumpad;
	}
    }

    FontLoader {id:fo; source: "FontAwesome.otf" }

    SystemPalette { id: palette }


    //value also gets modified by the delegates!
    onValueChanged: { loader.status == Loader.Ready?loader.item.updateValueString():{} }

    onOpened: { loader.status == Loader.Ready?loader.item.opened():{} }
    
    onLowerLimitChanged: {
        if (value < lowerLimit) {
            value = lowerLimit;
        }
    }
    onUpperLimitChanged: {
        if (value > upperLimit) {
            value = upperLimit;
        }
    }    
    onDecimalsChanged: { 
        decimals = Math.max(decimals,0);
    }
    /* FIXME is this function used ?
    function setValue(v) {
        if (v != value) {
            //digPos = 0;
            value = v;
        }
    }
*/
    Loader {
	id: loader
	sourceComponent: Pd.useNumpad?numEditDelegate:touchEditDelegate
    }


    // ---------------- classical touch edit --------------------------------------------
    Component {
	id: touchEditDelegate

	ColumnLayout {
	    id: delegate
	    spacing:10

	    Keys.onPressed: (event) => {
		if (event.key == Qt.Key_0) {
		    zeroAction.trigger();
		    event.accepted = true;
		}
		if(event.key == Qt.Key_Plus) {
		    Pd.useNumpad = !Pd.useNumpad;		    
		    event.accepted = true;
		}
	    }
	    Keys.onRightPressed: rightAction.trigger()
	    Keys.onLeftPressed: leftAction.trigger()
	    Keys.onUpPressed: upAction.trigger()
	    Keys.onDownPressed: downAction.trigger()
	    Keys.onReturnPressed: okAction.trigger()	    
	    
	    property int digPos: 0
	    onDigPosChanged: updateValueString();
	    
	    // -----------------------------------------------------------------
	    function isCharNumber(c){
		return c >= '0' && c <= '9';
	    }

	    // -----------------------------------------------------------------

	    function numNumbers(s) {
		let cnt = 0;
		for(let i = 0; i < s.length; i++) {
		    if(isCharNumber(s[i]))
			cnt++;
		}
		return cnt;
	    }
	    
	    // -----------------------------------------------------------------
	    function updateValueString() {

		var width = digPos + decimals + 1;

		var valueStr = value.toLocaleString(Qt.locale(),'f',decimals);

		//console.log("dig: " + digPos + " len: " + valueStr.length + " num Numbers: " + numNumbers(valueStr) + " decimals: " + decimals)

		//leading zeros if necessary
		while (numNumbers(valueStr) - decimals <= digPos) {
		    valueStr = "0" + valueStr;
		}

		//now fix negative values (set "-" to the beginning)
		if(valueStr.indexOf("-") > 0) {
		    valueStr= "-" + valueStr.replace('-', '');
		}

		if (valueStr.length > 0) {

		    var pos, digCount = 0;
		    var html = "";

		    for (pos = valueStr.length - 1; pos >= 0; pos--) {
			//console.log("pos: "+pos)
			if (isCharNumber(valueStr.charAt(pos))) {
			    //console.log("is int: "+valueStr.charAt(pos))

			    if (digPos + decimals == digCount) {
				html = "<span style=\""+
				    "color: blue; "+
				    "text-decoration: underline;"+
				    "\">" + valueStr.charAt(pos) + "</span>" + html;
			    } else {
				html = valueStr.charAt(pos) + html;
			    }

			    digCount++;
			} else {
			    html = valueStr.charAt(pos) + html;
			}
		    }

		    html = "<html><head><meta http-equiv=\"Content-Type\" "+
			"content=\"text/html; charset=utf-8\"></head><body>"+
			"<div align=\"center\" style=\""+
			"font-size: 24pt;"+
			"\">" + html + suffix + "</div></body></html>";
		    //console.log("html: "+html)
		    valueLabel.text = html;
		}

	    } 
	    // -----------------------------------------------------------------
	    function setEditDigit(dig) {
		console.log("set edit digit: " + dig)
		if (dig < -decimals) {
		    dig = -decimals;
		}

		if (upperLimit != Number.MAX_VALUE && lowerLimit != -Number.MAX_VALUE) {
		    var emax = Math.max(
			Math.floor(Math.log(Math.abs(upperLimit))/Math.log(10)),
			Math.floor(Math.log(Math.abs(lowerLimit))/Math.log(10)));
		    if (dig > emax) {
			dig = emax;
		    }
		}

		if (dig != digPos) {
		    digPos = dig;
		    console.log("digpos: "+digPos)
		    //updateValueString();
		}
	    }

	    // -----------------------------------------------------------------
	    function digitUp() {
		var digitValue = Math.pow(10, digPos);
		var eps = 0.5 * Math.pow(10, -decimals - digPos);
		var r = Math.floor(value / digitValue + eps) * digitValue;
		value = r + digitValue;
		if (value > upperLimit) {
		    value = upperLimit;
		}
	    }

	    // -----------------------------------------------------------------
	    function digitDown() {
		var digitValue = Math.pow(10, digPos);
		var eps = 0.5 * Math.pow(10, -decimals - digPos);
		var r = Math.ceil(value / digitValue - eps) * digitValue;
		value = r - digitValue;
		if (value < lowerLimit) {
		    value = lowerLimit;
		}
	    }


	    // -----------------------------------------------------------------
	    function setZero() {
		if (lowerLimit > 0.0) {
		    value = lowerLimit;
		}
		else if (upperLimit < 0.0) {
		    value = upperLimit;
		}
		else {
		    value = 0.0;
		}
	    }
	    Label {
		id:valueLabel
		padding:20
		text:"0"
		//font.pixelSize:40
		textFormat: Text.RichText
		horizontalAlignment: Qt.AlignHCenter
		verticalAlignment: Qt.AlignVCenter
		Layout.fillWidth:true
	    }
            //Button Grid
            GridLayout {
		columns:3
		columnSpacing:10
		rowSpacing:10
		Rectangle { //empty
                    width:1 //FIXME must be of size to be in the list
                    height:1
                    color:"transparent" //palette.window
		}
		Action {
		    id: upAction
		    onTriggered: delegate.digitUp()
		}
		Button {
                    id:up
                    font.family: "FontAwesome"
                    font.pixelSize: 20
                    text: "\uf062"
                    implicitHeight:70
                    implicitWidth:120
                    autoRepeat:true
		    action: upAction
		}
		Rectangle { //empty
                    width:1
                    height:1
                    color:"transparent"
                    
		}
		Action {
		    id: leftAction
		    onTriggered: delegate.setEditDigit(delegate.digPos+1)
		}
		Button {
                    id:left
                    font.family: "FontAwesome"
                    font.pixelSize: 20
                    text: "\uf060"
                    implicitHeight:up.implicitHeight
                    implicitWidth:up.implicitWidth
                    action: leftAction
		}
		Action {
		    id: zeroAction
		    onTriggered: delegate.setZero()
		}
		Button {
                    id:zero
                    text:"0"
                    font.pixelSize: 30
                    implicitHeight:up.implicitHeight
                    implicitWidth:up.implicitWidth
		    action: zeroAction
		}
		Action {
		    id: rightAction
		    onTriggered: delegate.setEditDigit(delegate.digPos-1)
		}
		Button {
                    id:right
                    font.family: "FontAwesome"
                    font.pixelSize: 20
                    text: "\uf061"
                    implicitHeight:up.implicitHeight
                    implicitWidth:up.implicitWidth
                    action: rightAction
		}
		Button {
                    id:cancel
                    text:"Cancel"
                    font.pixelSize: 20
                    implicitHeight:up.implicitHeight
                    implicitWidth:up.implicitWidth
                    onClicked: { dialog.reject() }
		}
		Action {
		    id: downAction
		    onTriggered: delegate.digitDown()
		}
		Button {
                    id:down
                    font.family: "FontAwesome"
                    font.pixelSize: 20
                    text: "\uf063"
                    implicitHeight:up.implicitHeight
                    implicitWidth:up.implicitWidth
                    autoRepeat:true
		    action: downAction
		}
		Action {
		    id: okAction
		    onTriggered: {
			if (updateValue != undefined) {
			    updateValue(value)
			}
			dialog.accept()
		    }
		}
		Button {
                    id:ok
                    text:"Ok"
                    font.pixelSize: 20
                    implicitHeight:up.implicitHeight
                    implicitWidth:up.implicitWidth
		    action: okAction
		}

            }
	    function opened() { updateValueString() }
	    Component.onCompleted:{
		updateValueString();
		delegate.forceActiveFocus();
	    }
	}
    } //~Classical Touch Edit

    // ------------------------------------------------------------------------
    // ------- Num Edit -------------------------------------------------------
    // ------------------------------------------------------------------------
    Component {
	id: numEditDelegate
	ColumnLayout {
	    spacing:10
	    id: delegate
	    property int btnWidth: 120
	    property int btnHeight: 70
	    property string valueString: ""

	    Keys.onPressed: (event) => {
		console.log("key event")
		let t = "";
		if (event.key >= Qt.Key_1 && event.key <= Qt.Key_9) {
		    t = (event.key - Qt.Key_0).toString();
		} else {
		    if (event.key == Qt.Key_Minus) {
			t = "-";
		    } else {
			if (event.key == Qt.Key_Comma || event.key == Qt.Key_Period) {
			    t = Qt.locale().decimalPoint;
			} 
		    }
		}
		if(t.length > 0) {
		    console.log("key event with key: " + t)
		    
		    for(let i=0; i < buttonRepeater.count; i++) {
			if (buttonRepeater.itemAt(i).text === t) {
			    buttonRepeater.itemAt(i).doClick();
			}
		    }
		    event.accepted = true;
		    return;
		}
		if(event.key == Qt.Key_0) {
		    zero.doClick();
		    event.accepted = true;
		}
		if(event.key == Qt.Key_Backspace || event.key == Qt.Key_Left) {
		    back.doClick();
		    event.accepted = true;
		}
		if(event.key == Qt.Key_Plus) {
		    Pd.useNumpad = !Pd.useNumpad;		    
		    event.accepted = true;
		}
		    
	    }
	    Keys.onReturnPressed: accept.doClick()	    
	    /*
	    Keys.onRightPressed: rightAction.trigger()
	    Keys.onLeftPressed: leftAction.trigger()
	    Keys.onUpPressed: upAction.trigger()
	    Keys.onDownPressed: downAction.trigger()
	    */
	    
	    // -----------------------------------------------------------------
	    function updateValueString() {
		valueString = value.toLocaleString(Qt.locale(),'f',decimals);
		valueLabel.text = valueString + suffix
	    }

	    function validate(s) {
		
		valueLabel.enabled = true

		let gs = Qt.locale().groupSeparator;

		valueString = s.split(gs).join(""); //delete all groupSeparator
		
		//console.log("valueLabel" + valueString+ "gs " + gs)
		
		if (valueString.length > 0) {
		    //Limit first
		    //place new groupSeparators (we do that only on the integerpart)
		    let parts = valueString.split(Qt.locale().decimalPoint);

		    let gsStr = "";

		    let k = 0;
		    //console.log("parts" + JSON.stringify(parts))
		    
		    for(let i = parts[0].length-1; i>=0; i--) {
			gsStr = parts[0][i] + gsStr;
			k++;
			if(k == 3 && (i>1 || (parts[0][0] != '-' && i>0))) {
			    gsStr = gs + gsStr;
			    k = 0;
			}
		    }
		    parts[0] = gsStr;
		    
		    if (parts.length > 1) {
			valueString = parts[0] + Qt.locale().decimalPoint + parts[1];
		    } else {
			valueString = parts[0];
		    }
		    
		}
		valueLabel.text = valueString + suffix
	    }

            Label {
		id:valueLabel
		//text: valueString + suffix
		font.pixelSize:30
		horizontalAlignment: Qt.AlignHCenter
		verticalAlignment: Qt.AlignVCenter
		Layout.fillWidth: true
	    }
	    Label {
		id: errorLabel
		color:"red"
		horizontalAlignment: Qt.AlignHCenter
		verticalAlignment: Qt.AlignVCenter
		Layout.fillWidth: true
		visible: false
	    }
	    
            //Button Grid

            GridLayout {
		columns:4
		columnSpacing:10
		rowSpacing:10
		Repeater {
		    id: buttonRepeater
		    model:["1","2","3",Qt.locale().decimalPoint,"4","5","6","-","7","8","9","Clear"]
		    Button {
			text: modelData
			font.pixelSize: 20
			font.bold:true
			implicitHeight: delegate.btnHeight
			implicitWidth: delegate.btnWidth
			autoRepeat:true

			function doClick() {
			    if (enabled) {
				if(text === "Clear")
				{ validate("");
				  errorLabel.visible = false
				}
				else
				    validate(valueString + text)  //only chars
			    }
			}
				
			onClicked: doClick()
			enabled: {
			    switch (text) {
			    case Qt.locale().decimalPoint:
				return !(valueString.indexOf(Qt.locale().decimalPoint) > -1 ||
					 valueString.length == 0 || decimals <= 0)
				break
			    case "-": //only at beginning
				return valueString.length == 0
				break
			    case "": return false
				break
			    case "Clear":
				return valueString.length > 0
				break
			    default:
				let idp = valueString.indexOf(Qt.locale().decimalPoint); //limit the number of digits after
				return !(idp > -1 && valueString.length - idp > dialog.decimals)
				break
			    }
			}
		    }
		}
		
		Button {
                    id:back
                    font.family: "FontAwesome"
                    font.pixelSize: 20
		    font.bold: true
                    text: "\uf060"
                    implicitHeight:delegate.btnHeight
                    implicitWidth:delegate.btnWidth
                    autoRepeat:true

		    function doClick() {
			if (enabled) {
			    validate(valueString.slice(0,-1));
			    errorLabel.visible = false
			}
		    }
			    
                    onClicked: doClick()
		    enabled: valueString.length > 0
		}
		Button {
                    id:zero
                    font.pixelSize: 20
                    text: "0"
		    font.bold: true
		    implicitHeight:delegate.btnHeight
                    implicitWidth:delegate.btnWidth
                    autoRepeat:true
		    function doClick() {
			if(enabled) {
			    validate(valueString + text);
			}
		    }
                    onClicked: doClick()
		    enabled: {
			let idp = valueString.indexOf(Qt.locale().decimalPoint); //limit the number of digits after
			return !(idp > -1 && valueString.length - idp > dialog.decimals)
		    }
		}
		Button {
                    id:accept
		    font.family: "FontAwesome"
                    font.pixelSize: 20
		    font.bold: true
                    text: "\uf00c"
		    implicitHeight:delegate.btnHeight
                    implicitWidth:delegate.btnWidth
                    autoRepeat:true
		    enabled: valueString.length > 1 || (valueString.length > 0 && valueString[0] != '-')
                    function doClick() {
			if (enabled && valueString.length > 0) {
			    let v = Number.fromLocaleString(Qt.locale(),valueString);
			    if (v > upperLimit) {
				errorLabel.text = "Der Wert überschreitet das obere Limit von: "+
				    upperLimit.toLocaleString(Qt.locale(),'f',decimals) + suffix;
				errorLabel.visible = true
			    } else {
				if (v < lowerLimit) {
				    errorLabel.text = "Der Wert unterschreitet das untere Limit von: "+
					lowerLimit.toLocaleString(Qt.locale(),'f',decimals) + suffix;
				    errorLabel.visible = true
				} else {
				    if(updateValue != undefined) {
					updateValue(v);
				    }
				    dialog.value = v;
				    dialog.accept()
				}
			    }
			}
		    }
		    onClicked:doClick()
		}
		Button {
                    font.pixelSize: 20
                    text: "Cancel"
		    font.bold: true
		    implicitHeight:delegate.btnHeight
                    implicitWidth:delegate.btnWidth
                    autoRepeat:true
                    onClicked: dialog.close()
		}
	    }

	    
	    function opened() { 
		//value has been set
		updateValueString()
		valueLabel.enabled = false
		//reset
		valueString = ""
		errorLabel.visible = false
	    }

	    Component.onCompleted: {
		opened();
		delegate.forceActiveFocus();
	    }
	}
    } // NumPad
} //Dialog
