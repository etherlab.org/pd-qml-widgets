import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 1.0

Label{
    id:control
    property var hash:[]
    property alias process: vector.process
    property alias path: vector.path
    property alias sampleTime:vector.sampleTime
    property bool startIndexFromOne:false
    property int index:-1 //if index is positive only the value of that index
                          //is chown (regardless off the mode)

    enum Mode {  //for vector elements
        All,     //show all elements
        Limits,  //show the max and minimum
        Cycle,    //cycle through the values
        Set      //show a set of distinct values
    }
    property int mode:PdVectorLabel.Mode.All
    property int indexOffset:startIndexFromOne ? 1:0


    Timer {
        id:timer
        interval: 800;
        repeat: true;
        running: false;
        property int i:0;
        onTriggered: {
            if(i > vector.value.length-1) {
                i = 0;
            }
            if(control.hash[vector.value[i]] != undefined) {
                control.text = (indexOffset+i)+": "+control.hash[vector.value[i]]
            } else {
                control.text = ""
            }
            i++;
        }
    }


    PdVector {
        id:vector
        onValueChanged:{
            var t = "";

            //cylce throw the different values 
            if(value.length == 1 || 
            (Math.min.apply(null,value) ==   //alle gleich
            Math.max.apply(null,value))) {  //only one value to output
                timer.running = false
                if(hash[value[0]] != undefined) {
                    control.text = hash[value[0]]
                } else {
                    control.text = ""
                }
            } else  {
                if (index > -1) { //auch scalar
                    if(hash[value[Math.min(index,value.length-1)]] != undefined) {
                        control.text = hash[value[Math.min(index,value.length-1)]]
                    } else {
                    control.text = ""

                    }

                } else { //different values
                    if(mode != PdVectorLabel.Mode.Cycle) {
                        timer.running = false
                    }

                    switch(mode) {
                        case PdVectorLabel.Mode.All:
                        for(var i=0;i<value.length;i++) {
                            if(hash[value[i]] != undefined) {
                                t+= hash[value[i]]
                                if(i != value.length -1) {
                                    t+=", ";
                                }
                            }
                        }
                        control.text = t;
                        break;

                        case PdVectorLabel.Mode.Limits: 
                        var 
                        imin = Math.min.apply(null,value),
                        imax = Math.max.apply(null,value);

                        if(hash[imin] != undefined) {
                            t=hash[imin];
                        }
                        if(hash[imax] != undefined) {
                            t = t + ' ... ' + hash[imax];
                        }
                        control.text = t;
                        break;
                        case PdVectorLabel.Mode.Cycle: 
                        timer.running = true;
                        break;

                        case PdVectorLabel.Mode.Set: 

                        var set = [];
                        var hit;
                        var first;
                        //FIXME geht sicher performanter
                        t = "{";
                            for(var i=0;i<value.length;i++) {
                                hit = false;
                                for(var j=0;j<set.length;j++) {
                                    if (value[i] == set[j]) {
                                        hit = true;
                                        break;
                                    }
                                }

                                if(hit == false) {
                                    set.push(value[i]);
                                }
                            }

                            set.sort(function(a, b){return a - b});
                            for(var i=0;i<set.length;i++) {
                                //mit Index
                                first = true;
                                for(var j=0;j<value.length;j++) {
                                    if(set[i] == value[j]) {
                                        if(!first) {
                                            t+=',';
                                        }
                                        t+=(j+indexOffset).toString();
                                        first = false;
                                    } //append the value
                                }
                                t+=": "+hash[set[i]];
                                if(i != set.length -1) {
                                    t+=";\n ";
                                }
                            }
                            t+= "}";
                        control.text = t;
                        break;
                    }
                }
            }
        }
    }
}
