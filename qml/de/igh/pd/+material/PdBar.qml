import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 2.0

ProgressBar {
    id:control
    property alias connection:scalar.connection
    property alias variable: scalar //direct interface to scalar
    property alias path: scalar.path
    property var suffix:""
    /** type:string
     * Prefix is appended to the value without space!
     */
    property var prefix:""
    property var decimals:2
    property bool startZero:true
    
    //------------ deprecated
    /*property alias process: scalar.process
    property alias scale: scalar.scale
    property alias sampleTime:scalar.sampleTime*/
    //------------
    property int orientation:Qt.Horizontal
    property alias labelVisible:label.visible
    property int fontPixelSize:12 //applies only for vertical orientation!!
    
    /** type:var
     * color of bar
     */
    property color color: Material.accent

    /** type:var
     * color of background
     */
    property color backgroundColor: Material.background

    /** type:var
     * color of border
     */
    property color borderColor: Material.foreground

    property alias label: label

    property int radius: 3
    
    value:scalar.value
    background: Rectangle {
        implicitWidth: 60
        implicitHeight: 20
        color:control.backgroundColor
        border.color: control.borderColor
        radius: control.radius
    }

    contentItem: Item {
        implicitWidth: orientation==Qt.Horizontal?100:20
        implicitHeight: orientation==Qt.Horizontal?20:100
	
        Rectangle {
            function scaleOrigin(l) {
		if(control.from < 0 && control.startZero) {
		    return (Math.min(control.visualPosition,
				     control.from/(control.from - control.to)) * l)
		} else {
		    return 0
		}
	    }
	    
            function scale(l) {
		if(control.from < 0 && control.startZero) {
		    return (Math.abs(control.visualPosition-
				     control.from/(control.from - control.to)) * l)
		} else {
		    return control.visualPosition * l
		}
	    }
            x: {
		if(control.orientation == Qt.Horizontal) {
		    return scaleOrigin(parent.width)
		} else {  //vertical
		     return 0
		}
            }

            width: {
		if(control.orientation == Qt.Horizontal) {
		    return scale(parent.width)
		} else {
		    return parent.width
		}
	    }
	    
	    y:{
		if(control.orientation == Qt.Horizontal) {
		    return 1
		} else {
		    return parent.height - height - scaleOrigin(parent.height) 
		}
	    }
            height: {
		if(control.orientation == Qt.Horizontal) {
		return parent.height-2
		} else {
		    return scale(parent.height)
		}
	    }
	    
            radius: 2
            color: control.color
            opacity:0.5
        }
        Label {
	    id:label
            anchors.centerIn:parent
            text:control.prefix + scalar.value.toLocaleString(Qt.locale(),'f',control.decimals) + control.suffix
	    
	    font.family: "Roboto" //monospace font für digitals

            font.pixelSize: {
		if(control.orientation == Qt.Horizontal) {
		    return control.height*8/10
		} else {
		    return control.fontPixelSize
		}
	    }
        }
    }
    PdScalar {
        id:scalar
    }
} //Progressbar 
