import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 2.0

//changes the appearence outline and filled from a pdvariable

ToolButton { 
    id:control
    property alias path: scalar.path
    property alias value: scalar.value
    property color backgroundColor:"#eeeeee"
    property color activeColor:Material.foreground
    property bool invert:false
    contentItem: Text {
        font: control.font
        text: control.text
        opacity: enabled ? 1.0 : 0.3
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        style: ((scalar.value != 0) != control.invert)? Text.Normal : Text.Outline
        color: ((scalar.value != 0) != control.invert)? control.activeColor : control.backgroundColor
    }

    enabled: scalar.connected

    PdScalar {
        id:scalar
    }
}
