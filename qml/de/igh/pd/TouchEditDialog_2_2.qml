/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* 
*
* TODO: Documentation
*
*
****************************************************************************/

import QtQuick 2.9           
import QtQuick.Layouts 1.3   
import QtQuick.Controls 2.3
import QtQml 2.12

import "Sprintf.js" as Str

/**
 * Dialog for editing numeric inputs via touch
 * code follows CursorEditWidget/TouchEditDialog in qtPdWidgets
 */

Dialog {
    id:dialog
    anchors.centerIn: Overlay.overlay
    /* FIXME this is not always running; don't know why? Hm
    parent: ApplicationWindow.overlay
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)
    */
    modal:true
    property double value:0
    property int decimals:2
    property int digPos:0
    property string suffix:""
    property double lowerLimit:-Number.MAX_VALUE
    property double upperLimit:Number.MAX_VALUE

    property var updateValue: undefined //callback for updating 
    
    header: Item {
        visible:(dialog.title.length > 0)? true:false
	implicitHeight:titleLabel.height*1.5
	Label {
	    anchors.centerIn:parent
	    topPadding:10
	    horizontalAlignment:Text.AlignHCenter
	    verticalAlignment:Text.AlignVCenter
            id:titleLabel
	    text:dialog.title
	    font.bold:true
	}
	RoundButton {
	    anchors.right:parent.right
	    anchors.top:parent.top
	    anchors.rightMargin:10
	    anchors.topMargin:10
	    text:"v"
	    onClicked: { Pd.useNumpad = true;
			 ToolTip.show("Die Eingabemethode wird beim nächsten Öffnen dieses Dialoges umgestellt.",3000)
		       }

	}
    }

    FontLoader {
        //source: "qrc:/de/igh/PdQmlWidgets/FontAwesome.otf"
        source: "FontAwesome.otf"
    }

    SystemPalette { id: palette }

    
    onValueChanged: { updateValueString() }
    onSuffixChanged: { updateValueString() }
    onLowerLimitChanged: {
        if (value < lowerLimit) {
            value = lowerLimit;
        }
    }
    onUpperLimitChanged: {
        if (value > upperLimit) {
            value = upperLimit;
        }
    }    
    onDecimalsChanged: { 
        decimals = Math.max(decimals,0);
        updateValueString();
    }
    function setValue(v) {
        if (v != value) {
            digPos = 0;
            value = v;
        }
    }

    onAboutToShow: digPos = 0;

    onDigPosChanged: updateValueString();
    
    // -----------------------------------------------------------------
    function isCharNumber(c){
        return c >= '0' && c <= '9';
    }

    // -----------------------------------------------------------------

    function numNumbers(s) {
	let cnt = 0;
	for(let i = 0; i < s.length; i++) {
	    if(isCharNumber(s[i]))
	       cnt++;
	}
	return cnt;
    }
    
    // -----------------------------------------------------------------
    function updateValueString() {

        var width = digPos + decimals + 1;

	var valueStr = value.toLocaleString(Qt.locale(),'f',decimals);

	//console.log("dig: " + digPos + " len: " + valueStr.length + " num Numbers: " + numNumbers(valueStr) + " decimals: " + decimals)

	//leading zeros if necessary
	while (numNumbers(valueStr) - decimals <= digPos) {
	    valueStr = "0" + valueStr;
	}

	//now fix negative values (set "-" to the beginning)
	if(valueStr.indexOf("-") > 0) {
	    valueStr= "-" + valueStr.replace('-', '');
	}

        if (valueStr.length > 0) {

            var pos, digCount = 0;
            var html = "";

            for (pos = valueStr.length - 1; pos >= 0; pos--) {
                //console.log("pos: "+pos)
                if (isCharNumber(valueStr.charAt(pos))) {
                    //console.log("is int: "+valueStr.charAt(pos))

                    if (digPos + decimals == digCount) {
                        html = "<span style=\""+
                        "color: blue; "+
                        "text-decoration: underline;"+
                        "\">" + valueStr.charAt(pos) + "</span>" + html;
                    } else {
                        html = valueStr.charAt(pos) + html;
                    }

                    digCount++;
                } else {
                    html = valueStr.charAt(pos) + html;
                }
            }

            html = "<html><head><meta http-equiv=\"Content-Type\" "+
            "content=\"text/html; charset=utf-8\"></head><body>"+
            "<div align=\"center\" style=\""+
            "font-size: 24pt;"+
            "\">" + html + suffix + "</div></body></html>";
            //console.log("html: "+html)
            valueLabel.text = html;
        }

    } 
    // -----------------------------------------------------------------
    function setEditDigit(dig) {
	console.log("set edit digit: " + dig)
        if (dig < -decimals) {
            dig = -decimals;
        }

        if (upperLimit != Number.MAX_VALUE && lowerLimit != -Number.MAX_VALUE) {
            var emax = Math.max(
            Math.floor(Math.log(Math.abs(upperLimit))/Math.log(10)),
            Math.floor(Math.log(Math.abs(lowerLimit))/Math.log(10)));
            if (dig > emax) {
                dig = emax;
            }
        }

        if (dig != digPos) {
            digPos = dig;
            console.log("digpos: "+digPos)
            //updateValueString();
        }
    }

    // -----------------------------------------------------------------
    function digitUp() {
        var digitValue = Math.pow(10, digPos);
        var eps = 0.5 * Math.pow(10, -decimals - digPos);
        var r = Math.floor(value / digitValue + eps) * digitValue;
        value = r + digitValue;
        if (value > upperLimit) {
            value = upperLimit;
        }
    }

    // -----------------------------------------------------------------
    function digitDown() {
        var digitValue = Math.pow(10, digPos);
        var eps = 0.5 * Math.pow(10, -decimals - digPos);
        var r = Math.ceil(value / digitValue - eps) * digitValue;
        value = r - digitValue;
        if (value < lowerLimit) {
            value = lowerLimit;
        }
    }


    // -----------------------------------------------------------------
    function setZero() {
        if (lowerLimit > 0.0) {
            value = lowerLimit;
        }
        else if (upperLimit < 0.0) {
            value = upperLimit;
        }
        else {
            value = 0.0;
        }
    }
    ColumnLayout {
	spacing:10
        Label {

	    /*
            background:Rectangle {
                color:palette.window
                radius:4
            }
*/
            id:valueLabel
            padding:20
            text:"0"
            //font.pixelSize:40
            textFormat: Text.RichText
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth:true
        }
        //Button Grid
        GridLayout {
            columns:3
            columnSpacing:10
            rowSpacing:10
            Rectangle { //empty
                width:1 //FIXME must be of size to be in the list
                height:1
                color:"transparent" //palette.window
            }
            Button {
                id:up
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf062"
                implicitHeight:70
                implicitWidth:120
                autoRepeat:true
                onClicked:{ dialog.digitUp() }
            }
            Rectangle { //empty
                width:1
                height:1
                color:"transparent"
                
            }
            Button {
                id:left
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf060"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked:{dialog.setEditDigit(dialog.digPos+1)}
            }
            Button {
                id:zero
                text:"0"
                font.pixelSize: 30
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked:{ dialog.setZero() }
            }
            Button {
                id:right
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf061"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked:{dialog.setEditDigit(dialog.digPos-1)}
            }
            Button {
                id:cancel
                text:"Cancel"
                font.pixelSize: 20
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked: { dialog.reject() }
            }
            Button {
                id:down
                font.family: "FontAwesome"
                font.pixelSize: 20
                text: "\uf063"
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                autoRepeat:true
                onClicked:{ dialog.digitDown() }
            }
            Button {
                id:ok
                text:"Ok"
                font.pixelSize: 20
                implicitHeight:up.implicitHeight
                implicitWidth:up.implicitWidth
                onClicked: {
		    if (updateValue != undefined) {
			updateValue(value)
		    }
		    dialog.accept()
		}
            }

        }
    }
    Component.onCompleted:{
	console.log("TouchEditDialog.2.2 is deprecated! Update your import to: de.igh.pd 2.3.")
        updateValueString()        
    }
} //Dialog
